﻿using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Configs
{
    internal class SystemConfig : ISystemConfig
    {
        /// <summary>
        /// Gets the name of the system.
        /// </summary>
        /// <value>
        /// The name of the system.
        /// </value>
        public string SystemName { get; set; }

        /// <summary>
        /// Gets the software version.
        /// </summary>
        /// <value>
        /// The software version.
        /// </value>
        public string SoftwareVersion { get; set; }
    }
}