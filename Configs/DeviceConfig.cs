﻿using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Configs
{
    /// <summary>
    /// Device config
    /// </summary>
    /// <seealso cref="EyeBrain.SightSync.Common.IDeviceConfig" />
    internal class DeviceConfig : IDeviceConfig
    {
        /// <summary>
        /// Gets or sets the screen distance.
        /// </summary>
        /// <value>
        /// The screen distance.
        /// </value>
        public long ScreenDistance { get; set; }

        /// <summary>
        /// Gets or sets the length of the screen.
        /// </summary>
        /// <value>
        /// The length of the screen.
        /// </value>
        public long ScreenLength { get; set; }

        /// <summary>
        /// Gets or sets the screen offset.
        /// </summary>
        /// <value>
        /// The screen offset.
        /// </value>
        public long ScreenOffset { get; set; }

        /// <summary>
        /// Gets or sets the camera separation.
        /// </summary>
        /// <value>
        /// The camera separation.
        /// </value>
        public long CameraSeparation { get; set; }

        /// <summary>
        /// Gets or sets the mirror angle.
        /// </summary>
        /// <value>
        /// The mirror angle.
        /// </value>
        public double MirrorAngle { get; set; }

        /// <summary>
        /// Gets or sets the screen angle.
        /// </summary>
        /// <value>
        /// The screen angle.
        /// </value>
        public double ScreenAngle { get; set; }

        /// <summary>
        /// Gets or sets the mirror screen distance.
        /// </summary>
        /// <value>
        /// The mirror screen distance.
        /// </value>
        public double MirrorScreenDistance { get; set; }
    }
}