﻿using System;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Configs
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="EyeBrain.SightSync.Common.IHardwareConfig" />
    internal class HardwareConfig : IHardwareConfig
    {
        /// <summary>
        /// Gets or sets the stepper motor COM port.
        /// </summary>
        /// <value>
        /// The stepper motor COM port.
        /// </value>
        public long StepperMotorCOMPort { get; set; }

        /// <summary>
        /// Gets or sets the left actuator instance.
        /// </summary>
        /// <value>
        /// The left actuator instance.
        /// </value>
        public long LeftActuatorInstance { get; set; }

        /// <summary>
        /// Gets or sets the right actuator instance.
        /// </summary>
        /// <value>
        /// The right actuator instance.
        /// </value>
        public long RightActuatorInstance { get; set; }

        /// <summary>
        /// Gets or sets the left phoropter address.
        /// </summary>
        /// <value>
        /// The left phoropter address.
        /// </value>
        public long LeftPhoropterAddress { get; set; } = 1;

        /// <summary>
        /// Gets or sets the right phoropter address.
        /// </summary>
        /// <value>
        /// The right phoropter address.
        /// </value>
        public long RightPhoropterAddress { get; set; } = 2;

        /// <summary>
        /// Gets or sets the motor step size chin rest.
        /// </summary>
        /// <value>
        /// The motor step size chin rest.
        /// </value>
        public long MotorStepSizeChinRest { get; set; } = 250;

        /// <summary>
        /// Gets or sets the motor target speed chin rest.
        /// </summary>
        /// <value>
        /// The motor target speed chin rest.
        /// </value>
        public long MotorTargetSpeedChinRest { get; set; } = 500;

        /// <summary>
        /// Gets or sets the chin motor address.
        /// </summary>
        /// <value>
        /// The chin motor address.
        /// </value>
        public long ChinMotorAddress { get; set; }
    }
}
