﻿using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Configs
{
    /// <summary>
    /// Screen Config
    /// </summary>
    /// <seealso cref="EyeBrain.SightSync.Common.IScreenConfig" />
    internal class ScreenConfig : IScreenConfig
    {
        /// <summary>
        /// Gets or sets the left vertical adjustment.
        /// </summary>
        /// <value>
        /// The left vertical adjustment.
        /// </value>
        public double LeftVerticalAdjustment { get; set; }
        
        /// <summary>
        /// Gets or sets the right vertical adjustment.
        /// </summary>
        /// <value>
        /// The right vertical adjustment.
        /// </value>
        public double RightVerticalAdjustment { get; set; }
        
        /// <summary>
        /// Gets or sets the left horizontal adjustment.
        /// </summary>
        /// <value>
        /// The left horizontal adjustment.
        /// </value>
        public double LeftHorizontalAdjustment { get; set; }
        
        /// <summary>
        /// Gets or sets the right horizontal adjustment.
        /// </summary>
        /// <value>
        /// The right horizontal adjustment.
        /// </value>
        public double RightHorizontalAdjustment { get; set; }
    }
}