﻿using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Configs
{
    /// <summary>
    /// Defines the config entry for the actuators
    /// </summary>
    /// <seealso cref=EyeBrain.SightSync.Common.IActuatorConfig />
    internal class ActuatorConfig : IActuatorConfig
    {
        /// <summary>
        /// Gets or sets the left gradient.
        /// </summary>
        /// <value>
        /// The left gradient.
        /// </value>
        public double LeftGradient { get; set; }

        /// <summary>
        /// Gets or sets the left intercept.
        /// </summary>
        /// <value>
        /// The left intercept.
        /// </value>
        public double LeftIntercept { get; set; }

        /// <summary>
        /// Gets or sets the right gradient.
        /// </summary>
        /// <value>
        /// The right gradient.
        /// </value>
        public double RightGradient { get; set; }

        /// <summary>
        /// Gets or sets the right intercept.
        /// </summary>
        /// <value>
        /// The right intercept.
        /// </value>
        public double RightIntercept { get; set; }

        /// <summary>
        /// Gets or sets the actuator speed percentage.
        /// </summary>
        /// <value>
        /// The actuator speed percentage.
        /// </value>
        public long ActuatorSpeedPercentage { get; set; }
        /// <summary>
        /// Gets or sets the actuator settle wait.
        /// </summary>
        /// <value>
        /// The actuator settle wait.
        /// </value>
        public long ActuatorSettleWait { get; set; }
        /// <summary>
        /// Gets or sets the actuator settle timeout.
        /// </summary>
        /// <value>
        /// The actuator settle timeout.
        /// </value>
        public long ActuatorSettleTimeout { get; set; }
        /// <summary>
        /// Gets or sets the actuator total devices.
        /// </summary>
        /// <value>
        /// The actuator total devices.
        /// </value>
        public long ActuatorTotalDevices { get; set; }
        /// <summary>
        /// Gets or sets the actuator total adc steps phoropter.
        /// </summary>
        /// <value>
        /// The actuator total adc steps phoropter.
        /// </value>
        public long ActuatorTotalAdcStepsPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator length phoropter.
        /// </summary>
        /// <value>
        /// The actuator length phoropter.
        /// </value>
        public long ActuatorLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator retract limit percentage.
        /// </summary>
        /// <value>
        /// The actuator retract limit percentage.
        /// </value>
        public long ActuatorRetractLimitPercentage { get; set; }
        /// <summary>
        /// Gets or sets the actuator extend limit percentage.
        /// </summary>
        /// <value>
        /// The actuator extend limit percentage.
        /// </value>
        public long ActuatorExtendLimitPercentage { get; set; }
        /// <summary>
        /// Gets or sets the actuator usable length phoropter.
        /// </summary>
        /// <value>
        /// The actuator usable length phoropter.
        /// </value>
        public long ActuatorUsableLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator usable minimum length phoropter.
        /// </summary>
        /// <value>
        /// The actuator usable minimum length phoropter.
        /// </value>
        public long ActuatorUsableMinimumLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator usable maximum length phoropter.
        /// </summary>
        /// <value>
        /// The actuator usable maximum length phoropter.
        /// </value>
        public long ActuatorUsableMaximumLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator accuracy phoropter.
        /// </summary>
        /// <value>
        /// The actuator accuracy phoropter.
        /// </value>
        public double ActuatorAccuracyPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator accuracy fudge value phoropter.
        /// </summary>
        /// <value>
        /// The actuator accuracy fudge value phoropter.
        /// </value>
        public double ActuatorAccuracyFudgeValuePhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator last chance settle wait.
        /// </summary>
        /// <value>
        /// The actuator last chance settle wait.
        /// </value>
        public long ActuatorLastChanceSettleWait { get; set; }
        /// <summary>
        /// Gets or sets the actuator COM port.
        /// </summary>
        /// <value>
        /// The actuator COM port.
        /// </value>
        public string ActuatorComPort { get; set; }
        /// <summary>
        /// Gets or sets the actuator device address phoropter le.
        /// </summary>
        /// <value>
        /// The actuator device address phoropter le.
        /// </value>
        public long ActuatorDeviceAddressPhoropterLE { get; set; }
        /// <summary>
        /// Gets or sets the actuator device address phoropter re.
        /// </summary>
        /// <value>
        /// The actuator device address phoropter re.
        /// </value>
        public long ActuatorDeviceAddressPhoropterRE { get; set; }
    }
}