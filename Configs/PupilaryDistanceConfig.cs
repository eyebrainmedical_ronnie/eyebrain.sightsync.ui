﻿using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Configs
{
    internal class PupilaryDistanceConfig : IPupilaryDistanceConfig
    {
        /// <summary>
        /// Gets or sets the gradient.
        /// </summary>
        /// <value>
        /// The gradient.
        /// </value>
        public double Gradient { get; set; }

        /// <summary>
        /// Gets or sets the intercept.
        /// </summary>
        /// <value>
        /// The intercept.
        /// </value>
        public double Intercept { get; set; }

        /// <summary>
        /// Gets or sets the left internal marker.
        /// </summary>
        /// <value>
        /// The left internal marker.
        /// </value>
        public double LeftInternalMarker { get; set; }

        /// <summary>
        /// Gets or sets the right internal marker.
        /// </summary>
        /// <value>
        /// The right internal marker.
        /// </value>
        public double RightInternalMarker { get; set; }
    }
}
