﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;
using Newtonsoft.Json;

namespace Eybrain.SightSynch.UI.Configs
{
    /// <summary>
    /// Represents the system configuration for the SightSync application
    /// </summary>
    /// <seealso cref="EyeBrain.SightSync.Common.IConfig" />
    /// <seealso cref="IConfig" />
    [FactoryType(typeof(IConfig), true)]
    internal class Config : IConfig
    {
        /// <summary>
        /// Gets or sets the device key.
        /// </summary>
        /// <value>
        /// The device key.
        /// </value>
        public Guid DeviceKey { get; set; }

        /// <summary>
        /// Gets the eye configuration location.
        /// </summary>
        /// <value>
        /// The eye configuration location.
        /// </value>
        public string EyeConfigLocation { get; set; } = @"\Sightsync Config\";

        /// <summary>
        /// Gets or sets the system configuration file.
        /// </summary>
        /// <value>
        /// The system configuration file.
        /// </value>
        public string SystemConfigurationFile { get; set; } = @"SightSync Configuration File.json";

        /// <summary>
        /// Gets or sets the left eye camera configuration file.
        /// </summary>
        /// <value>
        /// The left eye camera configuration file.
        /// </value>
        public string LeftEyeCameraConfigFile { get; set; } = "Left Camera Parameters - betav1.ini";

        /// <summary>
        /// Gets the right eye camera configuration file.
        /// </summary>
        /// <value>
        /// The right eye camera configuration file.
        /// </value>
        public string RightEyeCameraConfigFile { get; set; } = "Right Camera Parameters - betav1.ini";

        /// <summary>
        /// Gets or sets the left eye phoropter table file.
        /// </summary>
        /// <value>
        /// The left eye phoropter table file.
        /// </value>
        public string LeftEyePhoropterTableFile { get; set; } = "Left-PhoropterLookupTable.txt";

        /// <summary>
        /// Gets or sets the right eye phoropter table file.
        /// </summary>
        /// <value>
        /// The right eye phoropter table file.
        /// </value>
        public string RightEyePhoropterTableFile { get; set; } = "Right-PhoropterLookupTable.txt";

        /// <summary>
        /// Gets the pd.
        /// </summary>
        /// <value>
        /// The pd.
        /// </value>
        public IPupilaryDistanceConfig Pd { get; private set; }

        /// <summary>
        /// Gets the hardware.
        /// </summary>
        /// <value>
        /// The hardware.
        /// </value>
        public IHardwareConfig Hardware { get; private set; }

        /// <summary>
        /// Gets the actuator.
        /// </summary>
        /// <value>
        /// The actuator.
        /// </value>
        public IActuatorConfig Actuator { get; private set; }

        /// <summary>
        /// Gets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        public IDeviceConfig Device { get; private set; }

        /// <summary>
        /// Gets the system.
        /// </summary>
        /// <value>
        /// The system.
        /// </value>
        public ISystemConfig System { get; private set; }

        /// <summary>
        /// Gets the screen.
        /// </summary>
        /// <value>
        /// The screen.
        /// </value>
        public IScreenConfig Screen { get; private set; }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            var filename = Path.Combine(EyeConfigLocation, SystemConfigurationFile);
            if (!File.Exists(filename)) return;
            var values = JsonConvert.DeserializeObject<ExpandoObject>(File.ReadAllText(filename)) as dynamic;

            DeviceKey = Guid.Parse(values.DeviceKey);
            Pd = DynamicToStatic.ToStatic<PupilaryDistanceConfig>(values.Pd);
            Hardware = DynamicToStatic.ToStatic<HardwareConfig>(values.Hardware);
            Actuator = DynamicToStatic.ToStatic<ActuatorConfig>(values.Actuator);
            Device = DynamicToStatic.ToStatic<DeviceConfig>(values.Device);
            System = DynamicToStatic.ToStatic<SystemConfig>(values.System);
            Screen = DynamicToStatic.ToStatic<ScreenConfig>(values.Screen);
        }

        /// <summary>
        /// Static conversion class
        /// </summary>
        public static class DynamicToStatic
        {
            public static T ToStatic<T>(object expando)
            {
                var entity = Activator.CreateInstance<T>();

                //ExpandoObject implements dictionary
                var properties = expando as IDictionary<string, object>;

                if (properties == null)
                    return entity;

                foreach (var entry in properties)
                {
                    var propertyInfo = entity.GetType().GetProperty(entry.Key);
                    if (propertyInfo != null)
                        propertyInfo.SetValue(entity, entry.Value, null);
                }
                return entity;
            }
        }
    }
}
