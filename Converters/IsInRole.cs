﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Eybrain.SightSynch.UI.Converters
{
    public class IsInRole : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns <see langword="null" />, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var items = Instance.Off<string>(value)?.Split(',');
            return items != null && items.Any(role => Context.Active.Roles.Any(o => string.Compare(o, role, StringComparison.InvariantCultureIgnoreCase) == 0));
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns <see langword="null" />, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }

        /// <summary>
        /// Ins the role.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        public static bool InRole(string roles)
        {
            return (bool)new IsInRole().Convert(roles, typeof(bool), null, CultureInfo.CurrentCulture);
        }
    }
}
