﻿namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// The eyeBrain logo to display internally
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class InternalSplashViewModel : BaseViewModel
    {
        public InternalSplashViewModel()
        {
            
        }
    }
}
