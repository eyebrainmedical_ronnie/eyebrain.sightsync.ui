﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Eybrain.SightSynch.UI.Database;
using Eybrain.SightSynch.UI.Model;
using Newtonsoft.Json;

namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// Diagnose Images Capture during live operations of the system
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class TestRunsViewModel : BaseViewModel
    {
        private string _SourceDirectory;
        private RelayCommand<string> _SelectPathCommand;
        private RelayCommand<string> _NewTestRun;
        private ObservableCollection<RunResult> _Testruns;
        private RunResult _SelectedRun;


        /// <summary>
        /// Gets or sets the SelectedRun.
        /// </summary>
        /// <value>
        /// The SelectedRun value.
        /// </value>  
        public RunResult SelectedRun
        {
            get => _SelectedRun;
            set
            {
                _SelectedRun = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Testruns.
        /// </summary>
        /// <value>
        /// The Testruns value.
        /// </value>  
        public ObservableCollection<RunResult> Testruns
        {
            get => _Testruns ?? (_Testruns = new ObservableCollection<RunResult>());
            set
            {
                _Testruns = value;
                OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the Command for type NewTestRun.
        /// </summary>
        /// <value>
        /// The NewTestRun value.
        /// </value>  
        public RelayCommand<string> NewTestRun
        {
            get => _NewTestRun ?? (_NewTestRun = new RelayCommand<string>(OnNewTestRunExecute));
            set
            {
                _NewTestRun = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the SelectPathCommand.
        /// </summary>
        /// <value>
        /// The SelectPathCommand value.
        /// </value>
        public RelayCommand<string> SelectPathCommand
        {
            get => _SelectPathCommand ?? (_SelectPathCommand = new RelayCommand<string>(SelectDirectory));
            set
            {
                _SelectPathCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the SourceDirectory.
        /// </summary>
        /// <value>
        /// The SourceDirectory value.
        /// </value>  
        public string SourceDirectory
        {
            get => _SourceDirectory;
            set
            {
                _SourceDirectory = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when [activated].
        /// </summary>
        public override void OnActivated()
        {
            SetBusy("Loading Test Runs");
            Task.Run(async () =>
            {
                //var jsonPath = Path.Combine(Environment.GetEnvironmentVariable("LocalAppData"), @"Dropbox\info.json");
                //var detail = JsonConvert.DeserializeObject<ExpandoObject>(File.ReadAllText(jsonPath));
                //var dir = ((dynamic)detail).business.path;
                //var source = await Data.FirstOrDefault<string>("Control.GetStringValue", "ImageProcessing.SourceImagePath");
                //SourceDirectory = $"{dir}{source}";

                Testruns.Clear();
                // Load the test runs from the database
                var res = await Data.Fetch<RunResult>("Testruns.GetLatestRuns");
                res.ToList().ForEach(Testruns.Add);
                IsBusy = false;
            });
        }

        /// <summary>
        /// Selects the directory.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void SelectDirectory(object obj)
        {
            using (var fbd = new FolderBrowserDialog { SelectedPath = SourceDirectory, Description = "Select a different path" })
            {
                var result = fbd.ShowDialog();
                if (result != DialogResult.OK || string.IsNullOrWhiteSpace(fbd.SelectedPath)) return;
                SourceDirectory = fbd.SelectedPath;
            }
        }

        /// <summary>
        /// Called when NewTestRun command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnNewTestRunExecute(string value)
        {
            Nav.Go("performtestrun", SourceDirectory);
        }
    }
}
