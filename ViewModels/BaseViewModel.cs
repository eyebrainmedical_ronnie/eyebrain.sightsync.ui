﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Eybrain.SightSynch.UI.Annotations;

namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// Base View Model for all views in the system
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        private bool _IsBusy;
        private string _BusyMessage;
        private string _Title;
        
        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title value.
        /// </value>  
        public string Title
        {
            get => _Title;
            set
            {
                _Title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the BusyMessage.
        /// </summary>
        /// <value>
        /// The BusyMessage value.
        /// </value>  
        public string BusyMessage
        {
            get => _BusyMessage;
            set
            {
                _BusyMessage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the IsBusy.
        /// </summary>
        /// <value>
        /// The IsBusy value.
        /// </value>  
        public bool IsBusy
        {
            get => _IsBusy;
            set
            {
                _IsBusy = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Sets the busy.
        /// </summary>
        /// <param name="message">The message.</param>
        protected void SetBusy(string message)
        {
            IsBusy = true;
            BusyMessage = message;
        }

        /// <summary>
        /// Called when the view activated
        /// </summary>
        public virtual void OnActivated() { }

        /// <summary>
        /// Called when the view is deactivated.
        /// </summary>
        public virtual void OnDeactivated() { }

        #region Property Changed Notifications
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
