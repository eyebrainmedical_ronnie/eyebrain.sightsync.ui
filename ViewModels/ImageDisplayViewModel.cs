﻿namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// Displays an image on the control
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class ImageDisplayViewModel : BaseViewModel
    {
        private string _Filename;


        /// <summary>
        /// Gets or sets the Filename.
        /// </summary>
        /// <value>
        /// The Filename value.
        /// </value>  
        public string Filename
        {
            get => _Filename;
            set
            {
                _Filename = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageDisplayViewModel" /> class.
        /// </summary>
        /// <param name="filename">The filename.</param>
        public ImageDisplayViewModel(string filename)
        {
            Filename = filename;
        }
    }
}
