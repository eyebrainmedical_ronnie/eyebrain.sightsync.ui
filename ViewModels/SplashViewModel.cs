﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Threading;
using Eybrain.SightSynch.UI.Model;

namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// Represends the Splashview that displays as the system starts up
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class SplashViewModel : BaseViewModel
    {
        /// <summary>
        /// Called when [deactivated].
        /// </summary>
        public override void OnDeactivated()
        {
            // when the Splash screen clears, we have to clear the navigation graph
            Nav.ClearGraph();

#if DEBUG
            Context.Active = new User
            {
                Name = "Debug User",
                UserKey = Guid.Empty
            };

            Instance.Off<List<string>>(Context.Active.Roles).AddRange(new[] { "DOCTOR", "EYEBRAINADMIN" });
#endif

            Event.Fire(EventType.LogedIn, this, null);
        }
    }
}
