﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Eybrain.SightSynch.UI.Database;
using Eybrain.SightSynch.UI.Model;
using Eyebrain.SightSync.Framework;
//using EyeBrain.SightSync.Algorithms.EyeTracking.Detection.Purkinje.QuadrupleP1;
using EyeBrain.SightSync.Common;
using EyeBrain.SightSync.Detection;
//using EyeTech.Core.Primitives.Maths.Struct;
//using EyeTech.OpenCv.OpenCvSharp;
using Newtonsoft.Json;
using Image = System.Drawing.Image;

namespace Eybrain.SightSynch.UI.ViewModels
{
    public class PerformTestRunViewModel : BaseViewModel
    {
        private readonly string _SourcePath;
        private readonly List<string> _Files = new List<string>();
        private readonly Queue<string> _Remaining = new Queue<string>();
        private readonly object _Lock = new object();

        private Guid _TestRun = Guid.NewGuid();

        private RelayCommand<string> _PauseCommand;
        private RelayCommand<string> _StartCommand;
        private RelayCommand<string> _StopCommand;
        private bool _IsRunning;
        private bool _IsPaused;
        //private EyeTrackingQuadrupleP1 _EyeTracking;
        private Stopwatch _Stopwatch;
        private string _SoftwareVersion = "10.3";
        private bool _IsNotRunning;
        private int _CurrentFileNumber;
        private int _FileCount;
        private long _AverageTime;
        private double _SuccessRate;
        private double _FailureRate;
        private int _Formula = 1;
        private List<FormulaOption> _Formulas;


        /// <summary>
        /// Gets or sets the Formulas.
        /// </summary>
        /// <value>
        /// The Formulas value.
        /// </value>  
        public List<FormulaOption> Formulas
        {
            get => _Formulas ?? (_Formulas = new List<FormulaOption>());
            set
            {
                _Formulas = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Formula.
        /// </summary>
        /// <value>
        /// The Formula value.
        /// </value>  
        public int Formula
        {
            get => _Formula;
            set
            {
                _Formula = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets the FailureRate.
        /// </summary>
        /// <value>
        /// The FailureRate value.
        /// </value>  
        public double FailureRate
        {
            get => _FailureRate;
            set
            {
                _FailureRate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the SuccessRate.
        /// </summary>
        /// <value>
        /// The SuccessRate value.
        /// </value>  
        public double SuccessRate
        {
            get => _SuccessRate;
            set
            {
                _SuccessRate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the AverageTime.
        /// </summary>
        /// <value>
        /// The AverageTime value.
        /// </value>  
        public long AverageTime
        {
            get => _AverageTime;
            set
            {
                _AverageTime = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the FileCount.
        /// </summary>
        /// <value>
        /// The FileCount value.
        /// </value>  
        public int FileCount
        {
            get => _FileCount;
            set
            {
                _FileCount = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the CurrentFileNumber.
        /// </summary>
        /// <value>
        /// The CurrentFileNumber value.
        /// </value>  
        public int CurrentFileNumber
        {
            get => _CurrentFileNumber;
            set
            {
                _CurrentFileNumber = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the IsNotRunning.
        /// </summary>
        /// <value>
        /// The IsNotRunning value.
        /// </value>  
        public bool IsNotRunning
        {
            get => _IsNotRunning;
            set
            {
                _IsNotRunning = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the SoftwareVersion.
        /// </summary>
        /// <value>
        /// The SoftwareVersion value.
        /// </value>  
        public string SoftwareVersion
        {
            get => _SoftwareVersion;
            set
            {
                _SoftwareVersion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the IsPaused.
        /// </summary>
        /// <value>
        /// The IsPaused value.
        /// </value>  
        public bool IsPaused
        {
            get => _IsPaused;
            set
            {
                _IsPaused = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the IsRunning.
        /// </summary>
        /// <value>
        /// The IsRunning value.
        /// </value>  
        public bool IsRunning
        {
            get => _IsRunning;
            set
            {
                _IsRunning = value;
                IsNotRunning = !value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the StopCommand.
        /// </summary>
        /// <value>
        /// The StopCommand value.
        /// </value>  
        public RelayCommand<string> StopCommand
        {
            get => _StopCommand ?? (_StopCommand = new RelayCommand<string>(Stop));
            set
            {
                _StopCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the StartCommand.
        /// </summary>
        /// <value>
        /// The StartCommand value.
        /// </value>  
        public RelayCommand<string> StartCommand
        {
            get => _StartCommand ?? (_StartCommand = new RelayCommand<string>(Start));
            set
            {
                _StartCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the PauseCommand.
        /// </summary>
        /// <value>
        /// The PauseCommand value.
        /// </value>  
        public RelayCommand<string> PauseCommand
        {
            get => _PauseCommand ?? (_PauseCommand = new RelayCommand<string>(Pause));
            set
            {
                _PauseCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformTestRunViewModel" /> class.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        public PerformTestRunViewModel(string sourcePath)
        {
            _SourcePath = sourcePath;

            Formulas.Add(new FormulaOption { Item = 1, Description = "Formula 1" });
            Formulas.Add(new FormulaOption { Item = 2, Description = "Formula 2" });

            Formula = 2;
        }

        /// <summary>
        /// Called when [activated].
        /// </summary>
        public override async void OnActivated()
        {
            if (!Directory.Exists(_SourcePath))
            {
                Nav.Back();
                return;
            }
            //_EyeTracking = new EyeTrackingQuadrupleP1();
            _Stopwatch = new Stopwatch();
            IsRunning = false;
            await GetFilesRecursive(_SourcePath);
        }

        /// <summary>
        /// Called when [deactivated].
        /// </summary>
        public override void OnDeactivated()
        {
            _IsRunning = false;
            _IsPaused = false;
            //_EyeTracking = null;
            _Stopwatch.Stop();
            _Stopwatch = null;
            lock (_Lock)
            {
                _Remaining.Clear();
            }
            _Files.Clear();
        }

        /// <summary>
        /// Gets the files recursive.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <returns></returns>
        private async Task GetFilesRecursive(string directory)
        {
            var files = Directory.GetFiles(directory);
            _Files.AddRange(files);
            foreach (var dir in Directory.GetDirectories(directory))
            {
                await GetFilesRecursive(dir);
            }
            FileCount = _Files.Count;
        }

        /// <summary>
        /// Stops the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <exception cref="NotImplementedException"></exception>
        private void Stop(string obj)
        {
            _IsRunning = false;
            _IsPaused = false;
        }

        /// <summary>
        /// Pauses the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <exception cref="NotImplementedException"></exception>
        private void Pause(string obj)
        {
            _IsPaused = !_IsPaused;
        }

        /// <summary>
        /// Starts the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <exception cref="NotImplementedException"></exception>
        private async void Start(string obj)
        {
            var processingOption = new ProcessingOption();

            _TestRun = Guid.NewGuid();

            // Start by clearing the queue and then adding all the files to the code so that we can process them
            lock (_Lock)
            {
                _Remaining.Clear();
                _Files.ForEach(_Remaining.Enqueue);
            }

            if (_Files.Count <= 0) return;

            FileCount = _Files.Count;
            CurrentFileNumber = 0;

            await Data.Execute("Testrun.Create", _TestRun, DateTimeOffset.Now);
            IsRunning = true;
            var failed = 0;
            var sucess = 0;

            var detector = Factory.Make<IDetector>();

            while (IsRunning && _Remaining.Count > 0)
            {
                if (_IsPaused)
                {
                    await Task.Delay(50);
                    continue;
                }

                var file = NextFile();
                try
                {
                    CurrentFileNumber++;
                    var detail = new FileDetail(file);

                    if (!detail.IsValid)
                    {
                        // Invalid file, add it to the Database
                        await Data.Execute(
                            "Measurement.SaveFailed",
                            _TestRun,
                            DateTimeOffset.UtcNow,
                            file,
                            detail.SerialNumber,
                            detail.PatienId,
                            (int)detail.Eye,
                            detail.Measurement,
                            detail.Stage,
                            detail.PixelConversionRate,
                            detail.SourceSoftware + " " + detail.Version,
                            "",
                            false,
                            false,
                            false,
                            _Stopwatch.ElapsedMilliseconds
                        );
                        failed++;

                        File.Copy(file, $"d:\\testruns\\{Path.GetFileNameWithoutExtension(file)}_F{Formula}.bmp");
                    }
                    else
                    {
                        if (Formula == 2)
                        {
                            var inputImageMat = (Bitmap)Image.FromFile(file);

                            _Stopwatch.Restart();
                            var res = detector.Process(inputImageMat, detail.Eye == Eyes.Left ? EyeLocation.Left : EyeLocation.Right, detail.PixelConversionRate);
                            _Stopwatch.Stop();

                            if (res)
                            {
                                var im = inputImageMat.Clone(new Rectangle(0, 0, inputImageMat.Width, inputImageMat.Height), PixelFormat.Format24bppRgb);
                                foreach (var point in detector.Eye.PurkinjeSet.Purkinjes)
                                {
                                    im.DrawPurkinje((float)point.Center.X, (float)point.Center.Y, Color.Red, 1);
                                    im.DrawRectangle(point.Bounds.X, point.Bounds.Y, point.Bounds.Width, point.Bounds.Height, Color.Red, 1);
                                }

                                var center = detector.Eye.PurkinjeSet.AverageCenter;
                                im.DrawPurkinje((float)center.X, (float)center.Y, Color.Blue, 1, 15);

                                var bounds = detector.Eye.Pupil.Bounds;
                                im.DrawPurkinje((float)detector.Eye.Pupil.Center.X, (float)detector.Eye.Pupil.Center.Y, Color.Red, 1, 15);
                                im.DrawRectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height, Color.Red, 1);
                                
                                im.Save($"d:\\testruns\\{Path.GetFileNameWithoutExtension(file)}_F2.bmp");

                                im.Dispose();
                                sucess++;

                                await Data.Execute(
                                    "Measurement.SaveSuccess",
                                    _TestRun,
                                    DateTimeOffset.UtcNow,
                                    file,
                                    detail.SerialNumber,
                                    detail.PatienId,
                                    (int) detail.Eye,
                                    detail.Measurement,
                                    detail.Stage,
                                    detail.PixelConversionRate,
                                    detail.SourceSoftware + " " + detail.Version,
                                    detector.Eye.ToString(),
                                    true,
                                    true,
                                    true,
                                    _Stopwatch.ElapsedMilliseconds,
                                    detector.Eye.PurkinjeSet.HorizontalDistance,
                                    detector.Eye.HorizonatlRotaion
                                );
                            }
                            else
                            {
                                inputImageMat.Save($"d:\\testruns\\{Path.GetFileNameWithoutExtension(file)}_F2.bmp");

                                failed++;
                                await Data.Execute(
                                    "Measurement.SaveFailed",
                                    _TestRun,
                                    DateTimeOffset.UtcNow,
                                    file,
                                    detail.SerialNumber,
                                    detail.PatienId,
                                    (int) detail.Eye,
                                    detail.Measurement,
                                    detail.Stage,
                                    detail.PixelConversionRate,
                                    detail.SourceSoftware + " " + detail.Version,
                                    "",
                                    detector.Eye.PurkinjeSet.Detected,
                                    detector.Eye.Pupil.Detected,
                                    false,
                                    _Stopwatch.ElapsedMilliseconds
                                );
                            }

                            detector.Reset();
                        }

                        //if (Formula == 1)
                        //{
                        //    Mat inputImageMat;
                        //    using (var stream = File.OpenRead(file))
                        //    {
                        //        inputImageMat = Mat.FromStream(stream, ImreadModes.GrayScale);
                        //    }

                        //    var p1Params = new PurkinjeP1DetectionQuadrupleP1Params();
                        //    var pupilParams = new PupilDetectionQuadrupleP1Params();

                        //    // Use the mmToPixels extracted from fileName else use the user input value
                        //    var mmToPixels = detail.PixelConversionRate;

                        //    p1Params.mmToPixelsConversionFactor = mmToPixels;
                        //    p1Params.MinXDiffTol = processingOption.P1MinXDiffTol;
                        //    p1Params.MaxXDiffTol = processingOption.P1MaxXDiffTol;
                        //    p1Params.YDiffTol = processingOption.P1YDiffTol;
                        //    p1Params.XPairXDiffTol = processingOption.P1XPairXDiffTol;
                        //    p1Params.MinYPairDiffTol = processingOption.P1MinYPairDiffTol;
                        //    p1Params.MaxYPairDiffTol = processingOption.P1MaxYPairDiffTol;
                        //    p1Params.P1Constraints =
                        //        new PurkinjeP1DetectionQuadrupleP1Params.PurkinjeP1ConstraintsParams()
                        //        {
                        //            P1AreaMinimum = processingOption.P1AreaMinimum,
                        //            P1AreaMaximum = processingOption.P1AreaMaximum,
                        //            P1ThresholdLower = processingOption.P1ThresholdLower,
                        //            P1ThresholdUpper = processingOption.P1ThresholdUpper
                        //        };

                        //    pupilParams.mmToPixelsConversionFactor = mmToPixels;
                        //    pupilParams.DetectionBandWidth = processingOption.PupilDetectionBandWidth;
                        //    pupilParams.DetectionBandHeight = processingOption.PupilDetectionBandHeight;
                        //    pupilParams.PupilConstraints =
                        //        new PupilDetectionQuadrupleP1Params.PupilConstraintsParams()
                        //        {
                        //            PupilAreaMinimum = processingOption.PupilDetectionAreaMin,
                        //            PupilAreaMaximum = processingOption.PupilDetectionAreaMax
                        //        };

                        //    _Stopwatch.Restart();
                        //    _EyeTracking.DetectPupilAndP1Complete(out EyeTrackingQuadrupleP1Results eyeTrackingResults, out Mat diagnosticsImage, out Mat purkinjeP1Image, pupilParams, p1Params, inputImageMat);
                        //    _Stopwatch.Stop();

                        //    if (eyeTrackingResults.IsTrackingSuccessful())
                        //    {
                        //        var im = inputImageMat.Clone();
                        //        var diagImage = new Mat();
                        //        Cv2.CvtColor(im, diagImage, ColorConversionCodes.GRAY2RGB);
                        //        foreach (var point in eyeTrackingResults.PurkinjeP1DetectionResults.pxP1Points)
                        //        {
                        //            Cv2.Line(diagImage, new EyeTech.OpenCv.OpenCvSharp.Point(point.X, point.Y - 5), new EyeTech.OpenCv.OpenCvSharp.Point(point.X, point.Y + 5), Scalar.Cyan);
                        //            Cv2.Line(diagImage, new EyeTech.OpenCv.OpenCvSharp.Point(point.X - 5, point.Y), new EyeTech.OpenCv.OpenCvSharp.Point(point.X + 5, point.Y), Scalar.Cyan);
                        //            Cv2.Circle(diagImage, new EyeTech.OpenCv.OpenCvSharp.Point(point.X, point.Y), 3, Scalar.Cyan);
                        //        }

                        //        Cv2.Line(diagImage,
                        //            new EyeTech.OpenCv.OpenCvSharp.Point(eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.X, eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.Y - 5),
                        //            new EyeTech.OpenCv.OpenCvSharp.Point(eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.X, eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.Y + 5), Scalar.Cyan);
                        //        Cv2.Line(diagImage,
                        //            new EyeTech.OpenCv.OpenCvSharp.Point(eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.X - 5, eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.Y),
                        //            new EyeTech.OpenCv.OpenCvSharp.Point(eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.X + 5, eyeTrackingResults.PupilDetectionResults.pxPupilCenterPoint.Y), Scalar.Cyan);

                        //        Cv2.ImWrite($"d:\\testruns\\{Path.GetFileNameWithoutExtension(file)}_F1.bmp", diagImage);
                        //        diagImage.Dispose();
                        //        im.Dispose();

                        //        sucess++;
                        //        var left = eyeTrackingResults.PurkinjeP1DetectionResults.pxP1BoundRects.Min(m => m.Left);
                        //        var right = eyeTrackingResults.PurkinjeP1DetectionResults.pxP1BoundRects.Max(m => m.Left);

                        //        await Data.Execute(
                        //            "Measurement.SaveSuccess",
                        //            _TestRun,
                        //            DateTimeOffset.UtcNow,
                        //            file,
                        //            detail.SerialNumber,
                        //            detail.PatienId,
                        //            (int) detail.Eye,
                        //            detail.Measurement,
                        //            detail.Stage,
                        //            detail.PixelConversionRate,
                        //            detail.SourceSoftware + " " + detail.Version,
                        //            JsonConvert.SerializeObject(eyeTrackingResults),
                        //            true,
                        //            true,
                        //            eyeTrackingResults.IsTrackingSuccessful(),
                        //            _Stopwatch.ElapsedMilliseconds,
                        //            right - left,
                        //            eyeTrackingResults.HorizontalEyeRotation
                        //        );
                        //    }
                        //    else
                        //    {
                        //        Cv2.ImWrite($"d:\\testruns\\{Path.GetFileNameWithoutExtension(file)}_F2.bmp", inputImageMat);
                        //        failed++;
                        //        await Data.Execute(
                        //            "Measurement.SaveFailed",
                        //            _TestRun,
                        //            DateTimeOffset.UtcNow,
                        //            file,
                        //            detail.SerialNumber,
                        //            detail.PatienId,
                        //            (int) detail.Eye,
                        //            detail.Measurement,
                        //            detail.Stage,
                        //            detail.PixelConversionRate,
                        //            detail.SourceSoftware + " " + detail.Version,
                        //            JsonConvert.SerializeObject(eyeTrackingResults),
                        //            eyeTrackingResults.PurkinjeP1DetectionResults.P1Detected,
                        //            eyeTrackingResults.PupilDetectionResults.PupilDetected,
                        //            false,
                        //            _Stopwatch.ElapsedMilliseconds
                        //        );
                        //    }
                        //}

                        AverageTime += _Stopwatch.ElapsedMilliseconds;
                        AverageTime /= 2;
                    }

                    SuccessRate = sucess / (double)CurrentFileNumber;
                    FailureRate = failed / (double)CurrentFileNumber;
                }
                catch (Exception ex)
                {
                    File.Copy(file, $"d:\\testruns\\{Path.GetFileNameWithoutExtension(file)}_F{Formula}.bmp");

                    await Data.Execute(
                        "Measurement.SaveFailed",
                        _TestRun,
                        DateTimeOffset.UtcNow,
                        file,
                        "UNKNOWN",
                        "UNKNOWN",
                        -1,
                        "UNKOWN",
                        "UNKNOWN",
                        0,
                        "UNKNOWN",
                        "",
                        false,
                        false,
                        false,
                        0
                    );
                    failed++;
                    // Here is an error what is it
                }
            }

            SuccessRate = sucess / (double)FileCount;
            FailureRate = failed / (double)FileCount;
            IsRunning = false;
        }

        ///// <summary>
        ///// Draws the rectangle.
        ///// </summary>
        ///// <param name="image">The image.</param>
        ///// <param name="bounds">The bounds.</param>
        ///// <param name="color">The color.</param>
        //private void DrawRectangle(Mat image, Rect2d bounds, Scalar color)
        //{
        //    Cv2.Line(image,
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X, bounds.Y), 
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X + bounds.Width, bounds.Y),
        //        color
        //    );

        //    Cv2.Line(image,
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X + bounds.Width, bounds.Y),
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X + bounds.Width, bounds.Y + bounds.Height),
        //        color
        //    );

        //    Cv2.Line(image,
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X + bounds.Width, bounds.Y + bounds.Height),
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X, bounds.Y + bounds.Height),
        //        color
        //    );

        //    Cv2.Line(image,
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X, bounds.Y + bounds.Height),
        //        new EyeTech.OpenCv.OpenCvSharp.Point(bounds.X, bounds.Y),
        //        color
        //    );
        //}

        public static byte[] ConvertBitmapToBytes(BitmapSource bmpSource)
        {
            byte[] retVal = null;

            if (bmpSource != null)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    // This converts to bytes of the file content for the target image type.
                    BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bmpSource));
                    encoder.Save(memoryStream);
                    retVal = memoryStream.ToArray();
                }
            }

            return retVal;
        }

        public static BitmapImage ConvertToBitmap(RenderTargetBitmap bmpSource)
        {
            BitmapImage retVal = null;

            if (bmpSource != null)
            {
                BitmapImage image = new BitmapImage();
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bmpSource));

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    encoder.Save(memoryStream);
                    // Reset position after save to memory stream before EndInit or there will be an exception.
                    memoryStream.Position = 0;

                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = memoryStream;
                    image.EndInit();
                    image.Freeze();
                }
                retVal = image;
            }

            return retVal;
        }

        /// <summary>
        /// To the image.
        /// </summary>
        /// <param name="bmpImageFileBytes">The BMP image file bytes.</param>
        /// <returns></returns>
        public static BitmapSource ConvertBmpFileBytesToBitmap(byte[] bmpImageFileBytes)
        {
            BitmapSource r = null;

            try
            {
                // Convert string to byte then to a BitmapImage. Doing a UTF8.GetString test to check on empty or null array.
                if (bmpImageFileBytes != null && bmpImageFileBytes.Length > 0 && !String.IsNullOrEmpty(Encoding.UTF8.GetString(bmpImageFileBytes, 0, bmpImageFileBytes.Length)))
                {
                    using (var memoryStream = new MemoryStream(bmpImageFileBytes))
                    {
                        // Build the image from the bytes array.
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.StreamSource = memoryStream;
                        image.EndInit();
                        image.Freeze();
                        memoryStream.Close();

                        r = image;
                    }
                }
            }
            catch (IOException)
            {
                // IO proble so ignore it since we return null.
                if (Debugger.IsAttached)
                {
                    // At the moment, we don't know what LabView will throw so in debug mode, want to see what exception is being throw here.
                    Debug.Assert(false);
                }
            }
            catch (Exception)
            {
                if (Debugger.IsAttached)
                {
                    // At the moment, we don't know what LabView will throw so in debug mode, want to see what exception is being throw here.
                    Debug.Assert(false);
                }
            }

            return r;
        }

        /// <summary>
        /// Gets the next file from the remaining files queue, making sure that it is multi-thread safe
        /// </summary>
        /// <returns></returns>
        private string NextFile()
        {
            lock (_Lock)
            {
                return _Remaining.Dequeue();
            }
        }

        private class FileDetail
        {
            /// <summary>
            /// Gets or sets the source software.
            /// </summary>
            /// <value>
            /// The source software.
            /// </value>
            public string SourceSoftware { get; }

            /// <summary>
            /// Gets or sets the version.
            /// </summary>
            /// <value>
            /// The version.
            /// </value>
            public string Version { get; }

            /// <summary>
            /// Gets or sets the patien identifier.
            /// </summary>
            /// <value>
            /// The patien identifier.
            /// </value>
            public string PatienId { get; }

            /// <summary>
            /// Gets or sets the device serial number.
            /// </summary>
            /// <value>
            /// The serial number.
            /// </value>
            public string SerialNumber { get; private set; }

            /// <summary>
            /// Gets or sets the pixel conversion rate.
            /// </summary>
            /// <value>
            /// The pixel conversion rate.
            /// </value>
            public double PixelConversionRate { get; }

            /// <summary>
            /// Gets or sets the eye.
            /// </summary>
            /// <value>
            /// The eye.
            /// </value>
            public Eyes Eye { get; }

            /// <summary>
            /// Returns true if ... is valid.
            /// </summary>
            /// <value>
            ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
            /// </value>
            public bool IsValid { get; }

            /// <summary>
            /// Gets or sets the type of the measurement.
            /// </summary>
            /// <value>
            /// The type of the measurement.
            /// </value>
            public string Measurement { get; }

            /// <summary>
            /// Gets or sets the stage.
            /// </summary>
            /// <value>
            /// The stage.
            /// </value>
            public string Stage { get; }

            /// <summary>
            /// Initializes a new instance of the <see cref="FileDetail"/> class.
            /// </summary>
            /// <param name="filename">The filename.</param>
            public FileDetail(string filename)
            {
                FindDeviceId(filename);

                var nameonly = Path.GetFileNameWithoutExtension(filename);
                IsValid = false;
                if (string.IsNullOrWhiteSpace(nameonly)) return;

                if (nameonly.Contains(' '))
                {
                    SourceSoftware = "LabView";

                    // Labview device filename
                    // 170301 2-06-44 AM NoPatientID PD Measurement LEFT
                    var items = nameonly.Split(' '); // Split on space so that we get the underlying data
                    if (items.Length < 7) return;

                    // Lab view has two types of filenames:
                    if (items.Length == 7)
                    {
                        Version = "7";

                        // Still a valid image, but we don't have conversion factor, set it to default
                        // 170301 2-06-44 AM NoPatientID PD Measurement LEFT
                        // 0: 170301
                        // 1: 2-06-44
                        // 2: AM 
                        // 3: NoPatientID 
                        // 4: PD 
                        // 5: Measurement
                        // 6: LEFT

                        PatienId = items[3];
                        Eye = items[6].ToLower() == "left" ? Eyes.Left : Eyes.Right;
                        PixelConversionRate = 0.053;
                        Measurement = items[4] + "M"; // align the data with C# where they have PD as PDM  representing Pupilar Distance Measurement (PDM)
                        Stage = ""; // we don't know when this image was taken from the filename, only that it could have been during PD only
                        IsValid = true;
                    }

                    if (items.Length == 9)
                    {
                        Version = "9";

                        // We have conversion and other detail
                        // 170314 4-03-50 AM NoPatientID PD Measurement Far 0.053 LEFT
                        // 0: 170314 
                        // 1: 4-03-50 
                        // 2: AM 
                        // 3: NoPatientID 
                        // 4: PD 
                        // 5: Measurement
                        // 6: Far 
                        // 7: 0.053 
                        // 8: LEFT

                        PatienId = items[3];
                        Eye = items[8].ToLower() == "left" ? Eyes.Left : Eyes.Right;
                        PixelConversionRate = double.Parse(items[7]);
                        Measurement = items[4] + "M"; // align the data with C# where they have PD as PDM  representing Pupilar Distance Measurement (PDM)
                        Stage = items[6]; // in c# this is a stage number, here it is Near or Fine
                        IsValid = true;
                    }
                }
                else
                {
                    SourceSoftware = "C#";
                    Version = "0.1";
                    PatienId = "Unkown";

                    // C# Filename
                    var items = nameonly.Split('.');
                    if (items.Length < 3) return;

                    // 2017-05-30T17_05_20.9100981Z_PDM.1_RE
                    // 2017-07-05T18_32_16.8780903Z_PDM.0_0.0472_RE
                    // 2017-06-07T16_23_20.9816991Z_FA.0.0_RE

                    // 0: 2017-07-05T18_32_16
                    // 1: 8780903Z_PDM
                    //    0: 8780903Z
                    //    1: PDM
                    // 2: 0_0.0472_RE
                    //    0: 0
                    //    1: 0.0472
                    //    2: RE

                    var time = items[1].Split('_');

                    // Time has two parts, so if it is not there this is an invalid filename
                    if (time.Length < 2) return;

                    if (items.Length == 3)
                    {
                        var det = items[2].Split('_');
                        // detail has three items:
                        // 0 - This combined with 
                        // 1 - conversion factor
                        // 2 - Eye

                        if (det.Length < 2) return;

                        switch (det.Length)
                        {
                            case 2:
                                PixelConversionRate = 0.053; // we don't have conversion rate on these files
                                Eye = det[1].ToLower() == "le" ? Eyes.Left : Eyes.Right;
                                Measurement = time[1];
                                Stage = det[0];
                                IsValid = true;
                                break;

                            case 3:
                                // Set the resulting data and return valid
                                PixelConversionRate = double.Parse(det[1]);
                                Eye = det[2].ToLower() == "le" ? Eyes.Left : Eyes.Right;
                                Measurement = time[1];
                                Stage = det[0];
                                IsValid = true;
                                break;
                        }
                    }
                    else if (items.Length == 4)
                    {
                        var det = items[3].Split('_');
                        // detail has three items:
                        // 0 - This combined with 
                        // 1 - conversion factor
                        // 2 - Eye

                        if (det.Length < 2) return;

                        switch (det.Length)
                        {
                            case 2:
                                PixelConversionRate = 0.053; // we don't have conversion rate on these files
                                Eye = det[1].ToLower() == "le" ? Eyes.Left : Eyes.Right;
                                Measurement = time[1];
                                Stage = det[0];
                                IsValid = true;
                                break;

                            case 3:
                                // Set the resulting data and return valid
                                PixelConversionRate = double.Parse(det[1]);
                                Eye = det[2].ToLower() == "le" ? Eyes.Left : Eyes.Right;
                                Measurement = time[1];
                                Stage = det[0];
                                IsValid = true;
                                break;
                        }
                    }
                }
            }

            /// <summary>
            /// Finds the device identifier.
            /// </summary>
            /// <param name="filename">The filename.</param>
            private void FindDeviceId(string filename)
            {
                var items = filename.ToLower().Split('\\');
                SerialNumber = "UNKNOWN";
                foreach (var item in items)
                {
                    // BETA Device Serial numbers are B000
                    if (item[0] == 'b' &&
                        item.Length == 4)
                    {
                        // Beta device image
                        SerialNumber = item;
                    }
                }
            }
        }
    }

    public class FormulaOption
    {
        public int Item { get; set; }
        public string Description { get; set; }
    }
}
