﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eybrain.SightSynch.UI.Model;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;
using Microsoft.Win32;

namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// Allows the user to select images to show on either eye or both at the same time
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class ImageTestSetupViewModel : BaseViewModel
    {
        private ObservableCollection<ImageViewItem> _Images;
        private ObservableCollection<Tuple<string, EyeLocation>> _Locations;
        private bool _CanDelete;
        private RelayCommand<ImageViewItem> _DeleteCommand;
        private RelayCommand<string> _NextCommand;
        private RelayCommand<string> _LoadCommand;
        private bool _CanNext;
        
        /// <summary>
        /// Gets or sets the CanNext.
        /// </summary>
        /// <value>
        /// The CanNext value.
        /// </value>  
        public bool CanNext
        {
            get => _CanNext;
            set
            {
                _CanNext = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type LoadCommand.
        /// </summary>
        /// <value>
        /// The LoadCommand value.
        /// </value>  
        public RelayCommand<string> LoadCommand
        {
            get => _LoadCommand ?? (_LoadCommand = new RelayCommand<string>(OnLoadCommandExecute));
            set
            {
                _LoadCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type NextCommand.
        /// </summary>
        /// <value>
        /// The NextCommand value.
        /// </value>  
        public RelayCommand<string> NextCommand
        {
            get => _NextCommand ?? (_NextCommand = new RelayCommand<string>(OnNextCommandExecute));
            set
            {
                _NextCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type DeleteCommand.
        /// </summary>
        /// <value>
        /// The DeleteCommand value.
        /// </value>  
        public RelayCommand<ImageViewItem> DeleteCommand
        {
            get => _DeleteCommand ?? (_DeleteCommand = new RelayCommand<ImageViewItem>(OnDeleteCommandExecute));
            set
            {
                _DeleteCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the CanDelete.
        /// </summary>
        /// <value>
        /// The CanDelete value.
        /// </value>  
        public bool CanDelete
        {
            get => _CanDelete;
            set
            {
                _CanDelete = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Locations.
        /// </summary>
        /// <value>
        /// The Locations value.
        /// </value>  
        public ObservableCollection<Tuple<string, EyeLocation>> Locations
        {
            get => _Locations ?? (_Locations = new ObservableCollection<Tuple<string, EyeLocation>>());
            set
            {
                _Locations = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Gets or sets the Images to show during the test run
        /// </summary>
        /// <value>
        /// The Images value.
        /// </value>
        public ObservableCollection<ImageViewItem> Images
        {
            get => _Images ?? (_Images = new ObservableCollection<ImageViewItem>());
            set
            {
                _Images = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when the view is activated.
        /// </summary>
        public override void OnActivated()
        {
            Locations.Clear();
            Locations.Add(new Tuple<string, EyeLocation>("Left", EyeLocation.Left));
            Locations.Add(new Tuple<string, EyeLocation>("Right", EyeLocation.Right));
            Locations.Add(new Tuple<string, EyeLocation>("Both", EyeLocation.Both));

            Images.Clear();
            var images = Factory.Make<IStorage>().Get<List<ImageViewItem>>("Images") ?? new List<ImageViewItem>();
            foreach (var item in images)
            {
                item.Host = this;
                Images.Add(item);
            }
        }

        /// <summary>
        /// Called when DeleteCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnDeleteCommandExecute(ImageViewItem value)
        {
            if (value == null) // the user wants to clear all the images
            {
                Images.Clear();
                return;
            }

            Images.Remove(value);
            CanNext = Images.Count > 0;
        }

        /// <summary>
        /// Called when NextCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnNextCommandExecute(string value)
        {
            if (!CanNext) return;
            foreach (var item in Images)
            {
                item.Host = null;
            }
            Factory.Make<IStorage>().Add("Images", Images.ToList());
            Nav.Next();
        }

        /// <summary>
        /// Called when LoadCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnLoadCommandExecute(string value)
        {
            var ofd = new OpenFileDialog
            {
                Title = "Select the file(s) you want to show",
                Multiselect = true,
                Filter = "Windows Bitmap Files (*.bmp)|*.bmp|Jpg Files (*.jpg)|*.jpg|Jpeg Files (*.jpeg)|*.jpeg|Portable Network Graphics (*.png)|*.png|Targa Image File Format (*.tiff)|*.tiff"
            };

            var res = ofd.ShowDialog();
            if (!res.HasValue || !res.Value) return;

            foreach (var file in ofd.FileNames)
            {
                Images.Add(new ImageViewItem
                {
                    FileName = file,
                    Location = EyeLocation.Both,
                    Host = this
                });
            }

            CanNext = Images.Count > 0;
        }
    }
}
