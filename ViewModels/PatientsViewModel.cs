﻿using System.Collections.ObjectModel;
using System.Linq;
using Eybrain.SightSynch.UI.Database;
using Eybrain.SightSynch.UI.Model;
using Eyebrain.SightSync.Framework;

namespace Eybrain.SightSynch.UI.ViewModels
{
    public class PatientsViewModel : BaseViewModel
    {
        private ObservableCollection<Patient> _Patients;
        private Patient _SelectedPatient;
        private RelayCommand<string> _SearchCommand;
        private RelayCommand<string> _NewPatientCommand;
        private RelayCommand<string> _NextCommand;
        private bool _CanNext;
        private bool _ShowKeyboard;

        /// <summary>
        /// Gets or sets the ShowKeyboard.
        /// </summary>
        /// <value>
        /// The ShowKeyboard value.
        /// </value>  
        public bool ShowKeyboard
        {
            get => _ShowKeyboard;
            set
            {
                _ShowKeyboard = value;
                OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the CanNext.
        /// </summary>
        /// <value>
        /// The CanNext value.
        /// </value>  
        public bool CanNext
        {
            get => _CanNext;
            set
            {
                _CanNext = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type NextCommand.
        /// </summary>
        /// <value>
        /// The NextCommand value.
        /// </value>  
        public RelayCommand<string> NextCommand
        {
            get => _NextCommand ?? (_NextCommand = new RelayCommand<string>(OnNextCommandExecute));
            set
            {
                _NextCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type NewPatientCommand.
        /// </summary>
        /// <value>
        /// The NewPatientCommand value.
        /// </value>  
        public RelayCommand<string> NewPatientCommand
        {
            get => _NewPatientCommand ?? (_NewPatientCommand = new RelayCommand<string>(OnNewPatientCommandExecute));
            set
            {
                _NewPatientCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type SearchCommand.
        /// </summary>
        /// <value>
        /// The SearchCommand value.
        /// </value>  
        public RelayCommand<string> SearchCommand
        {
            get => _SearchCommand ?? (_SearchCommand = new RelayCommand<string>(OnSearchCommandExecute));
            set
            {
                _SearchCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the SelectedPatient.
        /// </summary>
        /// <value>
        /// The SelectedPatient value.
        /// </value>  
        public Patient SelectedPatient
        {
            get => _SelectedPatient;
            set
            {
                _SelectedPatient = value;
                CanNext = value != null;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Patients.
        /// </summary>
        /// <value>
        /// The Patients value.
        /// </value>  
        public ObservableCollection<Patient> Patients
        {
            get => _Patients ?? (_Patients = new ObservableCollection<Patient>());
            set
            {
                _Patients = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when [activated].
        /// </summary>
        public override void OnActivated()
        {
            SelectedPatient = Factory.Make<IStorage>().Get<Patient>("Patient");
            ShowKeyboard = true;
        }

        /// <summary>
        /// Called when SearchCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private async void OnSearchCommandExecute(string value)
        {
            ShowKeyboard = false;
            if (string.IsNullOrWhiteSpace(value) || IsBusy) return;
            SetBusy("Searching, please wait...");
            var patiens = (await Data.Fetch<Patient>("Patients.SearchByCriteria", value)).ToList();
            Patients.Clear();
            ShowKeyboard = !patiens.Any();
            foreach (var patient in patiens)
            {
                Patients.Add(patient);
            }
            IsBusy = false;
        }

        /// <summary>
        /// Called when NewPatientCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnNewPatientCommandExecute(string value)
        {
            Nav.Go("NewPatient");
        }

        /// <summary>
        /// Called when NextCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnNextCommandExecute(string value)
        {
            Factory.Make<IStorage>().Add("Patient", SelectedPatient);
            Nav.Next();
        }
    }
}
