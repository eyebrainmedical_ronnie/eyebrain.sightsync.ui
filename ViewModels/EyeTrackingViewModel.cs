﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Eybrain.SightSynch.UI.Formulas;
using Eybrain.SightSynch.UI.Model;
using Eyebrain.SightSync.Drivers;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;
using EyeBrain.SightSync.Detection;

namespace Eybrain.SightSynch.UI.ViewModels
{
    /// <summary>
    /// This is a view of both camera's, it will display the output from both and then show the tracking lines, futher saving the information coming from the tracking into a database table
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class EyeTrackingViewModel : BaseViewModel
    {
        private IStereoCameras _Cameras;
        private IEvents _Events;
        private IDetector _LeftEyeDetector;
        private IDetector _RightEyeDetector;
        private RelayCommand<string> _StartCommand;
        private RelayCommand<string> _StopCommand;
        private ImageSource _LeftImage;
        private ImageSource _RightImage;
        private Eye _LeftEye;
        private Eye _RightEye;
        private bool _RightEyeBusy;
        private bool _LeftEyeBusy;
        private int _Formula = 1;
        private double _LeftFps;
        private double _RightFps;
        private int _LeftCount = 0;
        private int _RightCount = 0;
        private DateTime _LeftLasTime = DateTime.Now;
        private DateTime _RightLastTime = DateTime.Now;
        private bool _IsRunning;
        private bool _IsNotRunning;
        private string _Message;
        private Thickness _LeftEyePupil;
        private Thickness _RightEyePupil;
        private Thickness _LeftEyePurkinje;
        private Thickness _RightEyePurkinje;
        private PatientEyeResult _Patient;
        private ImageViewItem _CurrentLeftEye;
        private ImageViewItem _CurrentRightEye;
        private List<ImageViewItem> _Images;
        private List<ImageViewItem> _LeftImages;
        private List<ImageViewItem> _RightImages;
        private int _LeftImageIndex;
        private int _RightImageIndex;
        private RelayCommand<string> _NextImageCommand;
        private RelayCommand<string> _PrevImageCommand;
        private int _SampleCount = 50;
        private IPhoropters _Phoropters;
        private double _LeftConversionFactor;
        private double _RightConversionFactor;
        private ICalculation _Calculation;

        #region Public Properties
        /// <summary>
        /// Gets or sets the SampleCount.
        /// </summary>
        /// <value>
        /// The SampleCount value.
        /// </value>  
        public int SampleCount
        {
            get => _SampleCount;
            set
            {
                _SampleCount = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type PrevImageCommand.
        /// </summary>
        /// <value>
        /// The PrevImageCommand value.
        /// </value>  
        public RelayCommand<string> PrevImageCommand
        {
            get => _PrevImageCommand ?? (_PrevImageCommand = new RelayCommand<string>(OnPrevImageCommandExecute));
            set
            {
                _PrevImageCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type NextImageCommand.
        /// </summary>
        /// <value>
        /// The NextImageCommand value.
        /// </value>  
        public RelayCommand<string> NextImageCommand
        {
            get => _NextImageCommand ?? (_NextImageCommand = new RelayCommand<string>(OnNextImageCommandExecute));
            set
            {
                _NextImageCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the CurrentRightEye.
        /// </summary>
        /// <value>
        /// The CurrentRightEye value.
        /// </value>  
        public ImageViewItem CurrentRightEye
        {
            get => _CurrentRightEye;
            set
            {
                _CurrentRightEye = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the CurrentLeftEye.
        /// </summary>
        /// <value>
        /// The CurrentLeftEye value.
        /// </value>  
        public ImageViewItem CurrentLeftEye
        {
            get => _CurrentLeftEye;
            set
            {
                _CurrentLeftEye = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RightEyePurkinje.
        /// </summary>
        /// <value>
        /// The RightEyePurkinje value.
        /// </value>  
        public Thickness RightEyePurkinje
        {
            get => _RightEyePurkinje;
            set
            {
                _RightEyePurkinje = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the LeftEyePurkinje.
        /// </summary>
        /// <value>
        /// The LeftEyePurkinje value.
        /// </value>  
        public Thickness LeftEyePurkinje
        {
            get => _LeftEyePurkinje;
            set
            {
                _LeftEyePurkinje = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RightEyePupil.
        /// </summary>
        /// <value>
        /// The RightEyePupil value.
        /// </value>  
        public Thickness RightEyePupil
        {
            get => _RightEyePupil;
            set
            {
                _RightEyePupil = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the LeftEyePupil.
        /// </summary>
        /// <value>
        /// The LeftEyePupil value.
        /// </value>  
        public Thickness LeftEyePupil
        {
            get => _LeftEyePupil;
            set
            {
                _LeftEyePupil = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        /// <value>
        /// The Message value.
        /// </value>  
        public string Message
        {
            get => _Message;
            set
            {
                _Message = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the IsNotRunning.
        /// </summary>
        /// <value>
        /// The IsNotRunning value.
        /// </value>  
        public bool IsNotRunning
        {
            get => _IsNotRunning;
            set
            {
                _IsNotRunning = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the IsRunning.
        /// </summary>
        /// <value>
        /// The IsRunning value.
        /// </value>  
        public bool IsRunning
        {
            get => _IsRunning;
            set
            {
                _IsRunning = value;
                IsNotRunning = !value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RightFps.
        /// </summary>
        /// <value>
        /// The RightFps value.
        /// </value>  
        public double RightFps
        {
            get => _RightFps;
            set
            {
                _RightFps = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the LeftFps.
        /// </summary>
        /// <value>
        /// The LeftFps value.
        /// </value>  
        public double LeftFps
        {
            get => _LeftFps;
            set
            {
                _LeftFps = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Formula.
        /// </summary>
        /// <value>
        /// The Formula value.
        /// </value>  
        public int Formula
        {
            get => _Formula;
            set
            {
                _Formula = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RightEye.
        /// </summary>
        /// <value>
        /// The RightEye value.
        /// </value>  
        public Eye RightEye
        {
            get => _RightEye;
            set
            {
                _RightEye = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the LeftEye.
        /// </summary>
        /// <value>
        /// The LeftEye value.
        /// </value>  
        public Eye LeftEye
        {
            get => _LeftEye;
            set
            {
                _LeftEye = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RightImage.
        /// </summary>
        /// <value>
        /// The RightImage value.
        /// </value>  
        public ImageSource RightImage
        {
            get => _RightImage;
            set
            {
                _RightImage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the LeftImage.
        /// </summary>
        /// <value>
        /// The LeftImage value.
        /// </value>  
        public ImageSource LeftImage
        {
            get => _LeftImage;
            set
            {
                _LeftImage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type StopCommand.
        /// </summary>
        /// <value>
        /// The StopCommand value.
        /// </value>  
        public RelayCommand<string> StopCommand
        {
            get => _StopCommand ?? (_StopCommand = new RelayCommand<string>(OnStopCommandExecute));
            set
            {
                _StopCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type StartCommand.
        /// </summary>
        /// <value>
        /// The StartCommand value.
        /// </value>
        public RelayCommand<string> StartCommand
        {
            get => _StartCommand ?? (_StartCommand = new RelayCommand<string>(OnStartCommandExecute));
            set
            {
                _StartCommand = value;
                OnPropertyChanged();
            }
        }
        #endregion

        /// <summary>
        /// Called when the view is activated
        /// </summary>
        public override void OnActivated()
        {
            var storage = Factory.Make<IStorage>();

            // Begin the pointers in the middle of the screen
            LeftEyePupil = new Thickness(548, 309, 0, 0);
            LeftEyePurkinje = new Thickness(548, 0, 0, 0);
            RightEyePupil = new Thickness(548, 309, 0, 0);
            RightEyePurkinje = new Thickness(548, 0, 0, 0);

            // Get the Patient and the images to display to the patient
            _Patient = Factory.Make<PatientEyeResult>(storage.Get<Patient>("Patient"));
            _Images = storage.Get<List<ImageViewItem>>("Images");

            // Retrieve all the required components to complete this functionality
            _Events = Factory.Make<IEvents>();
            _Cameras = Factory.Make<IStereoCameras>();
            _RightEyeDetector = Factory.Make<IDetector>();
            _LeftEyeDetector = Factory.Make<IDetector>();
            _Phoropters = Factory.Make<IPhoropters>();
            _Calculation = Factory.Make<ICalculation>();

            // Set the correct eye conversion factors
            //TODO: This will depend on the Phoropter current position, we still need to take that into account
            _LeftConversionFactor = _Phoropters.GetConversionFactorForEye(EyeLocation.Left, 0);
            _RightConversionFactor = _Phoropters.GetConversionFactorForEye(EyeLocation.Right, 0);

            // Display the images from the camera
            _Events.Register("Camera.LeftEyeImage", Thread.CurrentThread, LeftEyeImageArrived);
            _Events.Register("Camera.RightEyeImage", Thread.CurrentThread, RightEyeImageArrived);

            // Grab a frame from the camera and calculate
            _Events.Register("Camera.LeftEyeImage", Thread.CurrentThread, LeftEyeImageCalculate);
            _Events.Register("Camera.RightEyeImage", Thread.CurrentThread, RightEyeImageCalculate);

            // Once the calculation has happened, set the results into the eye buffers and move the screen pointers
            _Events.Register("Calculation.LeftEyeReady", Thread.CurrentThread, LeftEyeReady);
            _Events.Register("Calculation.RightEyeReady", Thread.CurrentThread, RightEyeReady);

            // Save the results once the buffers are filled with the required values
            _Events.Register("SaveResults", Thread.CurrentThread, SaveResults);

            // Get the sets of images to display on either eyes
            _LeftImages = _Images.Where(f => f.Location == EyeLocation.Left || f.Location == EyeLocation.Both).ToList();
            _RightImages = _Images.Where(f => f.Location == EyeLocation.Right || f.Location == EyeLocation.Both).ToList();

            // Display the first set
            _LeftImageIndex = _RightImageIndex = 0;
            CurrentLeftEye = _LeftImages[_LeftImageIndex];
            CurrentRightEye = _RightImages[_LeftImageIndex];
            Nav.ShowOnLeftEye("imagedisplay", CurrentLeftEye.FileName);
            Nav.ShowOnRightEye("imagedisplay", CurrentRightEye.FileName);

            // Last step is to start the cameras up and receive the image events
            _Cameras.Start();   // start the camera from sending images

            // Set the running state
            IsRunning = false;
            Message = "Ready...";
        }

        /// <summary>
        /// Called when the view is deactivated.
        /// </summary>
        public override void OnDeactivated()
        {
            // stop the cameras from sending capture events
            _Cameras.Stop();

            // Deregister for all the events
            _Events.Deregister("Camera.LeftEyeImage", LeftEyeImageArrived);
            _Events.Deregister("Camera.RightEyeImage", RightEyeImageArrived);
            _Events.Deregister("Camera.LeftEyeImage", LeftEyeImageCalculate);
            _Events.Deregister("Camera.RightEyeImage", RightEyeImageCalculate);
            _Events.Deregister("Calculation.LeftEyeReady", LeftEyeReady);
            _Events.Deregister("Calculation.RightEyeReady", RightEyeReady);
            _Events.Deregister("SaveResults", SaveResults);

            // reset the internal display to show the company logo
            Nav.ShowOnLeftEye("internalsplash");
            Nav.ShowOnRightEye("internalsplash");
        }

        /// <summary>
        /// Saves the results.
        /// </summary>
        /// <param name="o">The o.</param>
        private async void SaveResults(object o)
        {
            Message = "Saving...";

            for (var index = 0; index < SampleCount; index++)
            {
                Message = $"Saving {index + 1} of {SampleCount}...";

                var right = _Patient.RightEyeResults[index];
                var left = _Patient.LeftEyeResults[index];

                var result = new EyeTrackResult
                {
                    Filename = CurrentLeftEye.FileName,
                    EyeTrackResultKey = Guid.NewGuid(),
                    PatientKey = _Patient.State.PatientKey,
                    TestDate = DateTimeOffset.Now,

                    PurkinjeWidthLeft = left.PurkinjeSet.HorizontalDistance,
                    PurkinjeWidthRight = right.PurkinjeSet.HorizontalDistance,

                    PupilLeftDetected = left.Pupil.Detected,
                    PupilRightDetected = right.Pupil.Detected,

                    PupilCenterLeftX = left.Pupil.Center.X,
                    PupilCenterLeftY = left.Pupil.Center.Y,

                    PupilCenterRightX = right.Pupil.Center.X,
                    PupilCenterRightY = right.Pupil.Center.Y,

                    P1LeftDetected = left.PurkinjeSet.Detected,
                    P1RightDetected = right.PurkinjeSet.Detected,

                    PurkinjeLeftCenterX = left.PurkinjeSet.AverageCenter.X,
                    PurkinjeLeftCenterY = left.PurkinjeSet.AverageCenter.Y,

                    PurkinjeRightCenterX = right.PurkinjeSet.AverageCenter.X,
                    PurkinjeRightCenterY = right.PurkinjeSet.AverageCenter.Y,

                    RotationRight = right.HorizonatlRotaion,
                    RotationLeft = left.HorizonatlRotaion,

                    PupilarDistance = _Calculation.CalcPupilaryDistance(left.Pupil.Center.X, right.Pupil.Center.X, _LeftConversionFactor, _RightConversionFactor)
                };

                await result.SaveAsync();
            }

            Message = "Ready...";
            _Patient.Reset();
        }

        /// <summary>
        /// Called when StopCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnStopCommandExecute(string value)
        {
            IsRunning = false;
            Message = $"Showing ({_LeftImageIndex}/{_RightImageIndex}) Recording Stopped";
        }

        /// <summary>
        /// Called when StartCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnStartCommandExecute(string value)
        {
            IsRunning = true;
            Message = $"Showing ({_LeftImageIndex}/{_RightImageIndex}) " + (IsRunning ? "Recording" : "Not Recording");
        }

        /// <summary>
        /// Rights the eye ready.
        /// </summary>
        /// <param name="eye">The eye.</param>
        private void RightEyeReady(object eye)
        {
            RightEye = (Eye)eye;
            if (RightEye == null)
            {
                _RightEyeBusy = false;
                return;
            }

            if (RightEye.Pupil.Detected) RightEyePupil = new Thickness(RightEye.Pupil.Center.X, RightEye.Pupil.Center.Y, 0, 0);
            if (RightEye.PurkinjeSet.Detected) RightEyePurkinje = new Thickness(RightEye.PurkinjeSet.AverageCenter.X, 0, 0, 0);

            if (IsRunning && _Patient.RightCount < SampleCount)
            {
                _Patient.RightEyeResults.Add(RightEye);
                CheckComplete();
                Message = $"Showing ({_LeftImageIndex}/{_RightImageIndex}) [{_Patient.LeftCount}:{_Patient.RightCount}] " + (IsRunning ? "Recording" : "Not Recording");
            }

            _RightEyeBusy = false;
        }

        /// <summary>
        /// Lefts the eye ready.
        /// </summary>
        /// <param name="eye">The eye.</param>
        private void LeftEyeReady(object eye)
        {
            LeftEye = (Eye)eye;
            if (LeftEye == null)
            {
                _LeftEyeBusy = false;
                return;
            }

            if (LeftEye.Pupil.Detected) LeftEyePupil = new Thickness(LeftEye.Pupil.Center.X, LeftEye.Pupil.Center.Y, 0, 0);
            if (LeftEye.PurkinjeSet.Detected) LeftEyePurkinje = new Thickness(LeftEye.PurkinjeSet.AverageCenter.X, 0, 0, 0);

            if (IsRunning && _Patient.LeftCount < SampleCount)
            {
                _Patient.LeftEyeResults.Add(LeftEye);
                CheckComplete();
                Message = $"Showing ({_LeftImageIndex}/{_RightImageIndex}) [{_Patient.LeftCount}:{_Patient.RightCount}] " + (IsRunning ? "Recording" : "Not Recording");
            }

            _LeftEyeBusy = false;
        }

        /// <summary>
        /// Checks the complete.
        /// </summary>
        private void CheckComplete()
        {
            if (!IsRunning) return;
            if (_Patient.LeftCount < SampleCount || _Patient.RightCount < SampleCount) return;
            IsRunning = false;
            Message = "Saving results...";
            _Events.Fire("SaveResults", null);
        }

        /// <summary>
        /// Rights the eye image calculate.
        /// </summary>
        /// <param name="image">The image.</param>
        private void RightEyeImageCalculate(object image)
        {
            if (_RightEyeBusy) return; // skip this frame because we are already busy calculating
            _RightEyeBusy = true;
            Task.Factory.StartNew(() =>
            {
                _RightEyeDetector.Process((Bitmap)image, EyeLocation.Right, _RightConversionFactor);
                _Events.Fire("Calculation.RightEyeReady", _RightEyeDetector.Eye);
                _RightEyeDetector.Reset();
            });
        }

        /// <summary>
        /// Lefts the eye image calculate.
        /// </summary>
        /// <param name="image">The image.</param>
        private void LeftEyeImageCalculate(object image)
        {
            if (_LeftEyeBusy) return;
            _LeftEyeBusy = true;
            Task.Factory.StartNew(() =>
            {
                _LeftEyeDetector.Process((Bitmap)image, EyeLocation.Left, _LeftConversionFactor);
                _Events.Fire("Calculation.LeftEyeReady", _LeftEyeDetector.Eye);
                _LeftEyeDetector.Reset();
            });
        }

        /// <summary>
        /// Rights the eye image arrived.
        /// </summary>
        /// <param name="image">The image.</param>
        private void RightEyeImageArrived(object image)
        {
            _RightCount++;
            var diff = DateTime.Now - _RightLastTime;
            if (diff.TotalSeconds > 1)
            {
                RightFps = _RightCount / diff.TotalSeconds;
                _RightCount = 0;
                _RightLastTime = DateTime.Now;
            }
            RightImage = ((Bitmap)image).ToBitmapImage();
        }

        /// <summary>
        /// Lefts the eye image arrived.
        /// </summary>
        /// <param name="image">The image.</param>
        private void LeftEyeImageArrived(object image)
        {
            _LeftCount++;
            var diff = DateTime.Now - _LeftLasTime;
            if (diff.TotalSeconds > 1)
            {
                LeftFps = _LeftCount / diff.TotalSeconds;
                _LeftCount = 0;
                _LeftLasTime = DateTime.Now;
            }
            LeftImage = ((Bitmap)image).ToBitmapImage();
        }
        
        /// <summary>
        /// Called when NextImageCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnNextImageCommandExecute(string value)
        {
            _LeftImageIndex++;
            _RightImageIndex++;

            if (_LeftImageIndex >= _LeftImages.Count) _LeftImageIndex = 0;
            if (_RightImageIndex >= _RightImages.Count) _RightImageIndex = 0;

            CurrentLeftEye = _LeftImages[_LeftImageIndex];
            CurrentRightEye = _RightImages[_LeftImageIndex];

            Nav.ShowOnLeftEye("imagedisplay", CurrentLeftEye.FileName);
            Nav.ShowOnRightEye("imagedisplay", CurrentRightEye.FileName);

            Message = $"Showing ({_LeftImageIndex}/{_RightImageIndex}) " + (IsRunning ? "Recording" : "Not Recording");
        }

        /// <summary>
        /// Called when PrevImageCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnPrevImageCommandExecute(string value)
        {
            _LeftImageIndex--;
            _RightImageIndex--;

            if (_LeftImageIndex <= 0) _LeftImageIndex = _LeftImages.Count - 1;
            if (_RightImageIndex <= 0) _RightImageIndex = _RightImages.Count - 1;

            CurrentLeftEye = _LeftImages[_LeftImageIndex];
            CurrentRightEye = _RightImages[_LeftImageIndex];

            Nav.ShowOnLeftEye("imagedisplay", CurrentLeftEye.FileName);
            Nav.ShowOnRightEye("imagedisplay", CurrentRightEye.FileName);

            Message = $"Showing ({_LeftImageIndex}/{_RightImageIndex}) " + (IsRunning ? "Recording" : "Not Recording");
        }
    }
}
