﻿using System.Collections.ObjectModel;
using System.Linq;
using Eybrain.SightSynch.UI.Model;

namespace Eybrain.SightSynch.UI.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private ObservableCollection<NavItem> _Tiles;

        /// <summary>
        /// Gets or sets the Tiles.
        /// </summary>
        /// <value>
        /// The Tiles value.
        /// </value>
        public ObservableCollection<NavItem> Tiles
        {
            get => _Tiles ?? (_Tiles = new ObservableCollection<NavItem>());
            set
            {
                _Tiles = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when the view is activated to display
        /// </summary>
        public override void OnActivated()
        {
            Tiles.Clear();
            Nav.Routes.Values.Where(w => w.IsVisible).ToList().ForEach(Tiles.Add);
        }
    }
}
