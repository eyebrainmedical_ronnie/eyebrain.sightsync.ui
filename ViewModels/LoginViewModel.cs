﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Model;

namespace Eybrain.SightSynch.UI.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private string _Message;
        private string _Pin;
        private RelayCommand<string> _PinCommand;
        private RelayCommand<string> _RedoCommand;
        private RelayCommand<string> _LoginCommand;
        private string _PinSize;


        /// <summary>
        /// Gets or sets the PinSize.
        /// </summary>
        /// <value>
        /// The PinSize value.
        /// </value>  
        public string PinSize
        {
            get => _PinSize;
            set
            {
                _PinSize = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type LoginCommand.
        /// </summary>
        /// <value>
        /// The LoginCommand value.
        /// </value>  
        public RelayCommand<string> LoginCommand
        {
            get => _LoginCommand ?? (_LoginCommand = new RelayCommand<string>(OnLoginCommandExecute));
            set
            {
                _LoginCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type RedoCommand.
        /// </summary>
        /// <value>
        /// The RedoCommand value.
        /// </value>  
        public RelayCommand<string> RedoCommand
        {
            get => _RedoCommand ?? (_RedoCommand = new RelayCommand<string>(OnRedoCommandExecute));
            set
            {
                _RedoCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Command for type PinCommand.
        /// </summary>
        /// <value>
        /// The PinCommand value.
        /// </value>  
        public RelayCommand<string> PinCommand
        {
            get => _PinCommand ?? (_PinCommand = new RelayCommand<string>(OnPinCommandExecute));
            set
            {
                _PinCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when PinCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnPinCommandExecute(string value)
        {
            Pin += value;
            PinSize += "*";
        }

        /// <summary>
        /// Gets or sets the Pin.
        /// </summary>
        /// <value>
        /// The Pin value.
        /// </value>  
        public string Pin
        {
            get => _Pin;
            set
            {
                _Pin = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        /// <value>
        /// The Message value.
        /// </value>  
        public string Message
        {
            get => _Message;
            set
            {
                _Message = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Logins the asynchronous.
        /// </summary>
        /// <param name="pin">The pin.</param>
        /// <returns></returns>
        public async Task<bool> LoginAsync(string pin)
        {
            SetBusy("Security check in progress, please wait...");
            await Task.Delay(500);

#if DEBUG
            Context.Active = new User
            {
                Name = "Ronnie Barnard",
                UserKey = Guid.Empty
            };

            Instance.Off<List<string>>(Context.Active.Roles).AddRange(new [] { "DOCTOR", "EYEBRAINADMIN" });
#endif

            Event.Fire(EventType.LogedIn, this, null);
            IsBusy = false;
            Message = "";
            Nav.ClearGraph();
            Nav.Next();
            return true;
        }


        /// <summary>
        /// Called when LoginCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private async void OnLoginCommandExecute(string value)
        {
            await LoginAsync(Pin);
        }

        /// <summary>
        /// Called when RedoCommand command is executed.
        /// </summary>
        /// <param name="value">The value that is passed as a parameter</param>
        private void OnRedoCommandExecute(string value)
        {
            Pin = "";
            PinSize = "";
        }
    }
}
