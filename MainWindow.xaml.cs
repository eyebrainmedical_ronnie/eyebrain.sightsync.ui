﻿using Eybrain.SightSynch.UI.ViewModels;

namespace Eybrain.SightSynch.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Loaded += (sender, args) => Instance.Off<BaseViewModel>(DataContext).OnActivated();
            Unloaded += (sender, args) => Instance.Off<BaseViewModel>(DataContext).OnDeactivated();
        }
    }
}
