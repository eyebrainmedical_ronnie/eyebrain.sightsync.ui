﻿using System.Threading.Tasks;

namespace Eybrain.SightSynch.UI.Interfaces
{
    public interface IBaseCommand
    {
        /// <summary>
        /// Determines whether this instance can execute with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        ///   <c>true</c> if this instance can execute with the specified parameters; otherwise, <c>false</c>.
        /// </returns>
        bool CanExecute(params object[] parameters);

        /// <summary>
        /// Executes with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        Task<bool> ExecuteAsync(params object[] parameters);

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        int Order { get; set; }

        /// <summary>
        /// Disposes the asynchronous.
        /// </summary>
        /// <returns></returns>
        Task DisposeAsync();
    }
}