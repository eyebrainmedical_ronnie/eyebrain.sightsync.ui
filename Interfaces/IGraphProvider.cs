﻿using System.Threading.Tasks;

namespace Eybrain.SightSynch.UI.Interfaces
{
    public interface IGraphProvider
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();
    }
}