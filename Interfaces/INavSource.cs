﻿using System.Windows.Controls;
using Eybrain.SightSynch.UI.ViewModels;

namespace Eybrain.SightSynch.UI.Interfaces
{
    public interface INavSource
    {
        /// <summary>
        /// Gets or sets the current view.
        /// </summary>
        /// <value>
        /// The current view.
        /// </value>
        UserControl CurrentView { get; set; }

        /// <summary>
        /// Gets or sets the left eye.
        /// </summary>
        /// <value>
        /// The left eye.
        /// </value>
        UserControl LeftEye { get; set; }

        /// <summary>
        /// Gets or sets the right eye.
        /// </summary>
        /// <value>
        /// The right eye.
        /// </value>
        UserControl RightEye { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show back].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show back]; otherwise, <c>false</c>.
        /// </value>
        bool ShowBack { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is busy navigating.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is busy; otherwise, <c>false</c>.
        /// </value>
        bool IsNavigating { get; set; }
    }
}
