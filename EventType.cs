﻿namespace Eybrain.SightSynch.UI
{
    public enum EventType
    {
        GraphDoesNotExist,
        GraphCreateFailure,
        SystemInitializeComplete,
        LogingOut,
        LogedOut,
        LogedIn
    }
}