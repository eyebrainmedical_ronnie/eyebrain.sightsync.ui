﻿using System.Windows;
using System.Windows.Controls;
using Eybrain.SightSynch.UI.ViewModels;

namespace Eybrain.SightSynch.UI.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView
    {
        private bool _IsClearing;
        private bool _IsBusy;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginView"/> class.
        /// </summary>
        public LoginView()
        {
            InitializeComponent();
        }
    }
}
