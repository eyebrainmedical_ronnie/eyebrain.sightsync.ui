﻿namespace Eybrain.SightSynch.UI.Views
{
    /// <summary>
    /// Interaction logic for ImageDiagnosticsView.xaml
    /// </summary>
    public partial class TestRunsView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestRunsView"/> class.
        /// </summary>
        public TestRunsView()
        {
            InitializeComponent();
        }
    }
}
