﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eybrain.SightSynch.UI.Database
{
    public static class Data
    {
        /// <summary>
        /// The database server
        /// </summary>
        private static readonly NeurolensesEntities Server = new NeurolensesEntities();

        /// <summary>
        /// Fetches the specified statementname.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="statementname">The statementname.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> Fetch<T>(string statementname, params object[] parameters)
        {
            return await Server.Database.SqlQuery<T>(await GetStatement(statementname), parameters).ToListAsync();
        }

        /// <summary>
        /// Firsts the or default.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="statementname">The statementname.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static async Task<T> FirstOrDefault<T>(string statementname, params object[] parameters)
        {
            return await Server.Database.SqlQuery<T>(await GetStatement(statementname), parameters).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Executes the specified statementname there is no return result expected.
        /// </summary>
        /// <param name="statementname">The statementname.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static async Task<int> Execute(string statementname, params object[] parameters)
        {
            var statement = await GetStatement(statementname);
            return await Server.Database.ExecuteSqlCommandAsync(statement, parameters);
        }

        /// <summary>
        /// Gets the statement.
        /// </summary>
        /// <param name="statementname">The statementname.</param>
        /// <returns></returns>
        private static async Task<string> GetStatement(string statementname)
        {
            return await Server.Database.SqlQuery<string>("select top 1 statement from SqlStatements where StatementName = {0} order by version desc", statementname).FirstOrDefaultAsync();
        }
    }
}
