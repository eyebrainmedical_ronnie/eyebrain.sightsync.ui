﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Threading;

namespace Eybrain.SightSynch.UI
{
    /// <summary>
    /// Events system for the UI application
    /// </summary>
    public static class Event
    {
        private class EventItem
        {
            public Thread EventThread { get; set; }
            public Action<object, object> EventAction { get; set; }
        }

        /// <summary>
        /// The events
        /// </summary>
        private static readonly Dictionary<EventType, List<EventItem>> Events = new Dictionary<EventType, List<EventItem>>();

        /// <summary>
        /// Registers the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="action">The action.</param>
        public static void Register(EventType name, Action<object, object> action)
        {
            if (!Events.ContainsKey(name))
            {
                Events.Add(name, new List<EventItem>());
            }
            Events[name].Add(new EventItem {EventAction = action, EventThread = Thread.CurrentThread});
        }

        /// <summary>
        /// Fires the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="value">The value.</param>
        public static void Fire(EventType name, object sender, object value)
        {
            if (!Events.ContainsKey(name)) return;
            Events[name].ForEach(e =>
            {
                Dispatcher.FromThread(e.EventThread)?.Invoke(() => e.EventAction(sender, value));
            });
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public static void Clear()
        {
            Events.Clear();
        }
    }
}
