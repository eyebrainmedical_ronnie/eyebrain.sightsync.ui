﻿using System;
using System.Collections.Generic;

namespace Eybrain.SightSynch.UI
{
    public static class Factory
    {
        private static readonly Dictionary<Type, Type> Dictionary = new Dictionary<Type, Type>();

        /// <summary>
        /// Adds the specified interface type.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="impplementationType">Type of the impplementation.</param>
        public static void Add(Type interfaceType, Type impplementationType)
        {
            if (Dictionary.ContainsKey(interfaceType)) return;
            Dictionary.Add(interfaceType, impplementationType);
        }

        /// <summary>
        /// Removes the specified interfacetype.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        public static void Remove(Type interfaceType)
        {
            if (!Dictionary.ContainsKey(interfaceType)) return;
            Dictionary.Remove(interfaceType);
        }

        /// <summary>
        /// Makes the specified parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static T Make<T>(params object[] parameters)
        {
            var type = typeof(T);
            return (T)Make(type, parameters);
        }
        /// <summary>
        /// Makes the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static object Make(Type type, params object[] parameters)
        {
            return Activator.CreateInstance(Dictionary.ContainsKey(type) ? Dictionary[type] : type, parameters);
        }

        /// <summary>
        /// Ensures the loaded.
        /// </summary>
        public static void EnsureLoaded()
        {
        }
    }
}
