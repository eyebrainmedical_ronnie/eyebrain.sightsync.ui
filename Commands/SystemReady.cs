﻿using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Interfaces;

namespace Eybrain.SightSynch.UI.Commands
{
    /// <summary>
    /// Start the cameras first
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.Interfaces.IBaseCommand" />
    public class SystemReady : IBaseCommand
    {
        /// <summary>
        /// Determines whether this instance can execute with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        ///   <c>true</c> if this instance can execute with the specified parameters; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool CanExecute(params object[] parameters)
        {
            return true;
        }

        /// <summary>
        /// Executes with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<bool> ExecuteAsync(params object[] parameters)
        {
            await Task.Delay(500); // wait for 500 miliseconds for the system to settle and all the required tasks to warm up before we translate the system to ready mode
            Event.Fire(EventType.SystemInitializeComplete, this, null);
            return true;
        }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; } = 1000;

        /// <summary>
        /// Disposes the asynchronous.
        /// </summary>
        /// <returns></returns>
        public Task DisposeAsync()
        {
            return null;
        }
    }
}
