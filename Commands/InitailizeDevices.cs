﻿using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Interfaces;
using Eyebrain.SightSync.Drivers;
using Eyebrain.SightSync.Framework;

namespace Eybrain.SightSynch.UI.Commands
{
    /// <summary>
    /// Initalize all the devices
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.Interfaces.IBaseCommand" />
    public class InitailizeDevices : IBaseCommand
    {
        /// <summary>
        /// Determines whether this instance can execute with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        ///   <c>true</c> if this instance can execute with the specified parameters; otherwise, <c>false</c>.
        /// </returns>
        public bool CanExecute(params object[] parameters)
        {
            return true;
        }

        /// <summary>
        /// Executes with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<bool> ExecuteAsync(params object[] parameters)
        {
            // Initialize all the different components of the system
            await Factory.Make<IEyeActuator>().Initialize();
            await Factory.Make<IChinRest>().Initialize();
            await Factory.Make<IStereoCameras>().Initialize();
            await Factory.Make<IPhoropters>().Initialize();

            return true;
        }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; } = 20;

        /// <summary>
        /// Disposes the asynchronous.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Task DisposeAsync()
        {
            return Task.Run(() =>
            {
                Factory.Make<IPhoropters>().Dispose();
                Factory.Make<IStereoCameras>().Dispose();
                Factory.Make<IChinRest>().Dispose();
                Factory.Make<IEyeActuator>().Dispose();
            });
        }
    }
}
