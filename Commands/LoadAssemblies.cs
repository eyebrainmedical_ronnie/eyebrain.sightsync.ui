﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Interfaces;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Commands
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.Interfaces.IBaseCommand" />
    public class LoadAssemblies : IBaseCommand
    {
        private IntPtr _NanotechDll;
        private IntPtr _ActuonixDll;

        /// <summary>
        /// Determines whether this instance can execute with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        ///   <c>true</c> if this instance can execute with the specified parameters; otherwise, <c>false</c>.
        /// </returns>
        public bool CanExecute(params object[] parameters)
        {
            return true;
        }

        /// <summary>
        /// Executes with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public Task<bool> ExecuteAsync(params object[] parameters)
        {
            return Task.Run(() =>
            {
                Factory.LoadAssembly(typeof(Factory).Assembly);                                     // Load the factory functionality
                Factory.LoadAssembly(typeof(Nav).Assembly);                                         // Loads the current running assembly

                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.Drivers.dll"));          // Defines all the different drivers required in the system
                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.Motor.dll"));            // Base Motor implementation that allows the different types of motors to function

                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.Camera.dll"));           // Camera Component
                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.Detection.dll"));        // Defines the functionality to detect the Pupil and Purkinje's

                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.Phoropter.dll"));        // Phoropter Component
                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.ChinRest.dll"));         // Chin rest component
                Factory.LoadAssembly(Assembly.LoadFrom("Eyebrain.SightSync.EyeActuator.dll"));      // Actuator moving the eye assembly left or right

                // Load the correct set of Libraries for the _NanotechDll based on the operating system type 64 or x84
                _NanotechDll = Native.LoadLibrary(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Environment.Is64BitOperatingSystem ? "x64" : "x86", "rs485_com.dll"));
                if (_NanotechDll == IntPtr.Zero)
                {
                    // Bad issue here, we cannot continue as the Stepper motor DLL's did not load properly.
                    throw new Win32Exception("The Nanotech Drivers did not load. They are not present in the X86 or X64 folders. This is a bad installation, please contact Neurolenses Support for help");
                }

                _ActuonixDll = Native.LoadLibrary(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Environment.Is64BitOperatingSystem ? "x64" : "x86", "mpusbapi.dll"));
                if (_ActuonixDll == IntPtr.Zero)
                {
                    // Bad issue here, we cannot continue as the Stepper motor DLL's did not load properly.
                    throw new Win32Exception("The Actuonix Drivers did not load. They are not present in the X86 or X64 folders. This is a bad installation, please contact Neurolenses Support for help");
                }

                return true;
            });
        }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; } = 0;

        /// <summary>
        /// Disposes the asynchronous.
        /// </summary>
        /// <returns></returns>
        public Task DisposeAsync()
        {
            return Task.Run(() =>
            {
                if (_NanotechDll != IntPtr.Zero) Native.FreeLibrary(_NanotechDll);
                if (_ActuonixDll != IntPtr.Zero) Native.FreeLibrary(_ActuonixDll);
            });
        }
    }
}
