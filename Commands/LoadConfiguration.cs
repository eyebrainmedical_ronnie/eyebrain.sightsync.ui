﻿using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Interfaces;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Commands
{
    /// <summary>
    /// Load configuration settings from files into the system
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.Interfaces.IBaseCommand" />
    internal class LoadConfiguration : IBaseCommand
    {
        /// <summary>
        /// Determines whether this instance can execute with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        ///   <c>true</c> if this instance can execute with the specified parameters; otherwise, <c>false</c>.
        /// </returns>
        public bool CanExecute(params object[] parameters)
        {
            return true;
        }

        /// <summary>
        /// Executes with the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public Task<bool> ExecuteAsync(params object[] parameters)
        {
            return Task.Run(() =>
            {
                var config = Factory.Make<IConfig>();

                // Config is initialized here becuase it has to be done first before anything else in the system
                config.Initialize();

                return config.System != null &&
                       config.Actuator != null &&
                       config.Device != null &&
                       config.Hardware != null &&
                       config.Pd != null &&
                       config.Screen != null;
            });
        }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; } = 2;

        /// <summary>
        /// Disposes the asynchronous.
        /// </summary>
        /// <returns></returns>
        public Task DisposeAsync()
        {
            return null;
        }
    }
}