﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Interfaces;

namespace Eybrain.SightSynch.UI.Commands
{
    public static class CommandHandler
    {
        private static readonly List<IBaseCommand> CommandList = new List<IBaseCommand>();

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public static void Clear()
        {
            CommandList.Clear();
        }

        /// <summary>
        /// Adds the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public static void Add(IBaseCommand command)
        {
            CommandList.Add(command);
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public static Task Execute(params object[] parameters)
        {
            return Task.Run(async () =>
            {
                foreach (var command in CommandList.OrderBy(o => o.Order).Where(w => w.CanExecute(parameters)))
                {
                    await command.ExecuteAsync(parameters);
                }
            });
        }

        /// <summary>
        /// Disposes the asynchronous.
        /// </summary>
        /// <returns></returns>
        public static void Dispose()
        {
            CommandList.OrderBy(o => o.Order).ToList().ForEach(async f => await f.DisposeAsync());
        }
    }
}
