﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Eybrain.SightSynch.UI.Interfaces;
using Eybrain.SightSynch.UI.Model;
using Eybrain.SightSynch.UI.ViewModels;
using Eyebrain.SightSync.Framework;

namespace Eybrain.SightSynch.UI
{
    /// <summary>
    /// Represents the navigation system
    /// </summary>
    public class Nav
    {
        private static readonly Stack<NavItem> ViewStack = new Stack<NavItem>();
        private static INavSource Source;

        /// <summary>
        /// Gets the routes.
        /// </summary>
        /// <value>
        /// The routes.
        /// </value>
        public static Dictionary<string, NavItem> Routes { get; } = new Dictionary<string, NavItem>();

        /// <summary>
        /// Gets the commands.
        /// </summary>
        /// <value>
        /// The commands.
        /// </value>
        public static List<CommandItem> Commands { get; } = new List<CommandItem>();

        /// <summary>
        /// Gets the active route.
        /// </summary>
        /// <value>
        /// The active route.
        /// </value>
        public static NavItem ActiveRoute { get; private set; }

        /// <summary>
        /// Initializes the specified nav graph source.
        /// </summary>
        /// <typeparam name="TGraphProvider">The type of the graph provider.</typeparam>
        /// <param name="source">The source.</param>
        public static void Initialize<TGraphProvider>(INavSource source)
        {
            ViewStack.Clear();
            Source = source;
            ((IGraphProvider)Factory.Make<TGraphProvider>()).Initialize();
        }

        /// <summary>
        /// Adds the route.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="modelType">Type of the model.</param>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="next">The next.</param>
        /// <param name="isVisible">if set to <c>true</c> [is visible].</param>
        /// <param name="title">The title.</param>
        /// <param name="background">The background.</param>
        /// <param name="foreground">The foreground.</param>
        /// <param name="icon">The icon.</param>
        public static void AddRoute(string name, Type modelType, Type viewType, string next, bool isVisible = false, string title = "", SolidColorBrush background = null, SolidColorBrush foreground = null, string icon = "")
        {
            if (Routes.ContainsKey(name.ToLower())) return;
            Routes.Add(name, new NavItem { ModelType = modelType, ViewType = viewType, Next = next, Name = name.ToLower(), IsVisible = isVisible, Background = background, Foreground = foreground, Icon = icon, Title = title });
        }

        /// <summary>
        /// Goes the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        public static void Go(string name, params object[] parameters)
        {
            Source.IsNavigating = true;
            Task.Delay(10);
            if (!Routes.ContainsKey(name.ToLower()))
            {
                Event.Fire(EventType.GraphDoesNotExist, name, parameters);
                Source.IsNavigating = false;
                return;
            }

            var route = Routes[name.ToLower()];
            var model = Factory.Make(route.ModelType, parameters) as BaseViewModel;
            var view = Factory.Make(route.ViewType) as UserControl;

            if (model == null || view == null)
            {
                Event.Fire(EventType.GraphCreateFailure, name, null);
                Source.IsNavigating = false;
                return;
            }

            view.DataContext = model;
            ActiveRoute?.Model.OnDeactivated();
            ActiveRoute = route;
            route.Model = model;
            route.View = view;
            model.Title = route.Title.ToUpper();
            ActiveRoute.Model.OnActivated();
            ViewStack.Push(route);
            Source.CurrentView = view;
            CheckBack();
            Source.IsNavigating = false;
        }

        /// <summary>
        /// Shows the content on the left eye.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        public static void ShowOnLeftEye(string name, params object[] parameters)
        {
            var detail = GetViewModel(name, parameters);
            if (!detail.Item3) return;
            if (Source.LeftEye != null) Instance.Off<BaseViewModel>(Source.LeftEye.DataContext).OnDeactivated();
            Source.LeftEye = detail.Item1;
        }

        /// <summary>
        /// Shows the content on the right eye.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        public static void ShowOnRightEye(string name, params object[] parameters)
        {
            var detail = GetViewModel(name, parameters);
            if (!detail.Item3) return;
            if (Source.RightEye != null) Instance.Off<BaseViewModel>(Source.RightEye.DataContext).OnDeactivated();
            Source.RightEye = detail.Item1;
        }

        /// <summary>
        /// Gets the view and view model
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        private static Tuple<UserControl, BaseViewModel, bool> GetViewModel(string name, params object[] parameters)
        {
            if (!Routes.ContainsKey(name.ToLower()))
            {
                Event.Fire(EventType.GraphDoesNotExist, name, parameters);
                return new Tuple<UserControl, BaseViewModel, bool>(null, null, false);
            }

            var route = Routes[name.ToLower()];
            var model = Factory.Make(route.ModelType, parameters) as BaseViewModel;
            var view = Factory.Make(route.ViewType) as UserControl;

            if (model == null || view == null)
            {
                Event.Fire(EventType.GraphCreateFailure, name, null);
                return new Tuple<UserControl, BaseViewModel, bool>(null, null, false);
            }

            view.DataContext = model;
            model.Title = route.Title.ToUpper();
            model.OnActivated();
            return new Tuple<UserControl, BaseViewModel, bool>(view, model, true);
        }

        /// <summary>
        /// Moves the view stack one view back
        /// </summary>
        public static void Back()
        {
            if (ViewStack.Count < 1) return;
            Source.IsNavigating = true;
            Task.Delay(10);
            ViewStack.Pop();
            ActiveRoute.Model.OnDeactivated();
            ActiveRoute.Model = null;
            ActiveRoute.View = null;
            ActiveRoute = null;

            if (ViewStack.Count < 1)
            {
                Source.IsNavigating = false;
                return;
            }

            var item = ViewStack.Peek();
            ActiveRoute = item;
            ActiveRoute.Model.OnActivated();
            Source.CurrentView = item.View;
            CheckBack();
            Source.IsNavigating = false;
        }

        /// <summary>
        /// Takes the viewstack all the way back to the bottom (Homepage)
        /// </summary>
        public static void Home()
        {
            Source.IsNavigating = true;
            Task.Delay(10);
            while (ViewStack.Count > 1)
            {
                var stackitem = ViewStack.Pop();
                stackitem.Model.OnDeactivated();
                stackitem.Model = null;
                stackitem.View = null;
            }

            ActiveRoute = ViewStack.Peek();
            ActiveRoute.Model.OnActivated();
            Source.CurrentView = ActiveRoute.View;
            CheckBack();
            Source.IsNavigating = false;
        }

        /// <summary>
        /// Deinitializeds this instance.
        /// </summary>
        /// <returns></returns>
        public static void Deinitialized()
        {
            Home();
            ViewStack.Clear();
            Routes.Clear();
        }

        /// <summary>
        /// Clears the routes.
        /// </summary>
        public static void ClearRoutes()
        {
            Routes.Clear();
        }

        /// <summary>
        /// Clears the graph.
        /// </summary>
        public static void ClearGraph()
        {
            ViewStack.Clear();
        }

        /// <summary>
        /// Nexts the asynchronous.
        /// </summary>
        /// <returns></returns>
        public static void Next(params object[] parameters)
        {
            if (string.IsNullOrWhiteSpace(ActiveRoute.Next)) return;
            Go(ActiveRoute.Next, parameters);
        }

        /// <summary>
        /// Adds the command.
        /// </summary>
        /// <param name="commandItem">The command item.</param>
        public static void AddCommand(CommandItem commandItem)
        {
            Commands.Add(commandItem);
        }

        /// <summary>
        /// Checks the back.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private static void CheckBack()
        {
            Source.ShowBack = ViewStack.Count > 1;
        }
    }
}
