﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Formulas
{
    [FactoryType(typeof(ICalculation), true)]
    internal class Calculations : ICalculation
    {
        /// <summary>
        /// The configuration for the system
        /// </summary>
        private readonly IConfig _Config;

        /// <summary>
        /// Initializes a new instance of the <see cref="Calculations" /> class.
        /// </summary>
        public Calculations()
        {
            _Config = Factory.Make<IConfig>();
        }

        /// <summary>
        /// Calculates the pupilary distance.
        /// </summary>
        /// <param name="leftpupilx">The leftpupilx.</param>
        /// <param name="rightpupilx">The rightpupilx.</param>
        /// <param name="leftConversionFactor">The left conversion factor.</param>
        /// <param name="rightConversionFactor">The right conversion factor.</param>
        /// <returns>
        /// Pupilary Distance measured in MM
        /// </returns>
        public double CalcPupilaryDistance(double leftpupilx, double rightpupilx, double leftConversionFactor, double rightConversionFactor)
        {
            var leftdifference = _Config.Pd.LeftInternalMarker - leftpupilx;
            var rightdifference = _Config.Pd.RightInternalMarker - rightpupilx;
            var scaledleft = leftdifference * leftConversionFactor;
            var scaledright = rightdifference * rightConversionFactor;
            var measureddiff = (scaledleft + _Config.Device.CameraSeparation) - scaledright;
            return (measureddiff * _Config.Pd.Gradient) + _Config.Pd.Intercept;
        }
    }

    internal interface ICalculation
    {
        /// <summary>
        /// Calculates the pupilary distance between two pupil centers
        /// </summary>
        /// <param name="leftpupilx">The leftpupilx.</param>
        /// <param name="rightpupilx">The rightpupilx.</param>
        /// <param name="leftConversionFactor">The left conversion factor.</param>
        /// <param name="rightConversionFactor">The right conversion factor.</param>
        /// <returns>Pupilary Distance measured in MM</returns>
        double CalcPupilaryDistance(double leftpupilx, double rightpupilx, double leftConversionFactor, double rightConversionFactor);
    }
}
