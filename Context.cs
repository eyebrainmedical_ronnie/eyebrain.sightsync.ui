﻿using Eybrain.SightSynch.UI.Model;

namespace Eybrain.SightSynch.UI
{
    public static class Context
    {
        /// <summary>
        /// Gets or sets the active.
        /// </summary>
        /// <value>
        /// The active.
        /// </value>
        public static User Active {get; set; }
    }
}
