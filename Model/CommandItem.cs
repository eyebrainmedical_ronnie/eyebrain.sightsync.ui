using System;

namespace Eybrain.SightSynch.UI.Model
{
    public class CommandItem
    {
        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public RelayCommand<object> Command { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [must be logged in].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [must be logged in]; otherwise, <c>false</c>.
        /// </value>
        public bool MustBeLoggedIn { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandItem" /> class.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="mustBeLoggedIn">if set to <c>true</c> [must be logged in].</param>
        public CommandItem(Action<object> action, string icon, bool mustBeLoggedIn = true)
        {
            MustBeLoggedIn = mustBeLoggedIn;
            Command = new RelayCommand<object>(action);
            Icon = icon;
        }
    }
}