using System;
using System.Windows.Controls;
using System.Windows.Media;
using Eybrain.SightSynch.UI.ViewModels;

namespace Eybrain.SightSynch.UI.Model
{
    public class NavItem
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the type of the model.
        /// </summary>
        /// <value>
        /// The type of the model.
        /// </value>
        public Type ModelType { get; set; }
        
        /// <summary>
        /// Gets or sets the type of the view.
        /// </summary>
        /// <value>
        /// The type of the view.
        /// </value>
        public Type ViewType { get; set; }
        
        /// <summary>
        /// Gets or sets the view.
        /// </summary>
        /// <value>
        /// The view.
        /// </value>
        public UserControl View { get; set; }
        
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public BaseViewModel Model { get; set; }

        /// <summary>
        /// Gets or sets the next.
        /// </summary>
        /// <value>
        /// The next.
        /// </value>
        public string Next { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is visible; otherwise, <c>false</c>.
        /// </value>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets or sets the background.
        /// </summary>
        /// <value>
        /// The background.
        /// </value>
        public SolidColorBrush Background { get; set; }

        /// <summary>
        /// Gets or sets the foreground.
        /// </summary>
        /// <value>
        /// The foreground.
        /// </value>
        public SolidColorBrush Foreground { get; set; }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public RelayCommand<string> Command { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NavItem"/> class.
        /// </summary>
        public NavItem()
        {
            Command = new RelayCommand<string>(Execute);
        }

        /// <summary>
        /// Executes the specified s.
        /// </summary>
        /// <param name="item">The item.</param>
        private void Execute(string item)
        {
            if (string.IsNullOrWhiteSpace(Name)) return;
            Nav.Go(Name);
        }

    }
}