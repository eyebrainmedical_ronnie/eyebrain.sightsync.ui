﻿namespace Eybrain.SightSynch.UI.Model
{
    public class ProcessingOption
    {
        /// <summary>
        /// Gets or sets the mm to pixels.
        /// </summary>
        /// <value>
        /// The mm to pixels.
        /// </value>
        public double MMToPixels { get; set; }

        /// <summary>
        /// Gets or sets the p1 minimum x difference tol.
        /// </summary>
        /// <value>
        /// The p1 minimum x difference tol.
        /// </value>
        public double P1MinXDiffTol { get; set; }

        /// <summary>
        /// Gets or sets the p1 maximum x difference tol.
        /// </summary>
        /// <value>
        /// The p1 maximum x difference tol.
        /// </value>
        public double P1MaxXDiffTol { get; set; }

        /// <summary>
        /// Gets or sets the p1 y difference tol.
        /// </summary>
        /// <value>
        /// The p1 y difference tol.
        /// </value>
        public double P1YDiffTol { get; set; }

        /// <summary>
        /// Gets or sets the p1 x pair x difference tol.
        /// </summary>
        /// <value>
        /// The p1 x pair x difference tol.
        /// </value>
        public double P1XPairXDiffTol { get; set; }

        /// <summary>
        /// Gets or sets the p1 minimum y pair difference tol.
        /// </summary>
        /// <value>
        /// The p1 minimum y pair difference tol.
        /// </value>
        public double P1MinYPairDiffTol { get; set; }

        /// <summary>
        /// Gets or sets the p1 maximum y pair difference tol.
        /// </summary>
        /// <value>
        /// The p1 maximum y pair difference tol.
        /// </value>
        public double P1MaxYPairDiffTol { get; set; }

        /// <summary>
        /// Gets or sets the p1 area minimum.
        /// </summary>
        /// <value>
        /// The p1 area minimum.
        /// </value>
        public double P1AreaMinimum { get; set; }

        /// <summary>
        /// Gets or sets the p1 area maximum.
        /// </summary>
        /// <value>
        /// The p1 area maximum.
        /// </value>
        public double P1AreaMaximum { get; set; }

        /// <summary>
        /// Gets or sets the p1 threshold lower.
        /// </summary>
        /// <value>
        /// The p1 threshold lower.
        /// </value>
        public double P1ThresholdLower { get; set; }

        /// <summary>
        /// Gets or sets the p1 threshold upper.
        /// </summary>
        /// <value>
        /// The p1 threshold upper.
        /// </value>
        public double P1ThresholdUpper { get; set; }

        /// <summary>
        /// Gets or sets the width of the pupil detection band.
        /// </summary>
        /// <value>
        /// The width of the pupil detection band.
        /// </value>
        public double PupilDetectionBandWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the pupil detection band.
        /// </summary>
        /// <value>
        /// The height of the pupil detection band.
        /// </value>
        public double PupilDetectionBandHeight { get; set; }

        /// <summary>
        /// Gets or sets the pupil detection area minimum.
        /// </summary>
        /// <value>
        /// The pupil detection area minimum.
        /// </value>
        public double PupilDetectionAreaMin { get; set; }

        /// <summary>
        /// Gets or sets the pupil detection area maximum.
        /// </summary>
        /// <value>
        /// The pupil detection area maximum.
        /// </value>
        public double PupilDetectionAreaMax { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessingOption"/> class.
        /// </summary>
        public ProcessingOption()
        {
            MMToPixels = 0.053; // all of these just follow LabView's and (likely) using 0 diopter lens
            PupilDetectionBandWidth = 18.33;
            PupilDetectionBandHeight = 1.137;
            PupilDetectionAreaMin = 2.27;
            PupilDetectionAreaMax = 10.75;
            P1MinXDiffTol = 4;
            P1MaxXDiffTol = 7.47;
            P1YDiffTol = 0.4;
            P1XPairXDiffTol = 0.4;
            P1MinYPairDiffTol = 1.79;
            P1MaxYPairDiffTol = 2.76;
            P1AreaMinimum = 0.014;
            P1AreaMaximum = 0.8;
            P1ThresholdLower = 190;
            P1ThresholdUpper = 255;
        }
    }
}
