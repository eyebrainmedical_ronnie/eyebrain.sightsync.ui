﻿namespace Eybrain.SightSynch.UI.Model
{
    public abstract class TrackingBase<T>
    {
        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public T State { get; private set; }

        /// <summary>
        /// Gets the old state.
        /// </summary>
        /// <value>
        /// The old state.
        /// </value>
        public T OldState { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackingBase{T}"/> class.
        /// </summary>
        /// <param name="state">The state.</param>
        protected TrackingBase(T state)
        {
            State = state;
            OldState = state;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackingBase{T}"/> class.
        /// </summary>
        protected TrackingBase()
        {
            State = default(T);
            OldState = default(T);
        }

        /// <summary>
        /// Sets the state.
        /// </summary>
        /// <param name="state">The state.</param>
        protected void SetState(T state)
        {
            State = state;
            OldState = state;
        }
    }
}
