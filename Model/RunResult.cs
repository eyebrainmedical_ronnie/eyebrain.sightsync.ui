using System;
using Eybrain.SightSynch.UI.ViewModels;

namespace Eybrain.SightSynch.UI.Model
{
    public class RunResult : BaseViewModel
    {
        private Guid _TestRunKey;
        private DateTime _RunDate;
        private int _Total;
        private int _PupilSuccessTotal;
        private int _P1SuccessTotal;
        private int _TotalSuccess;
        private int _Failed;
        private int _Speed;


        /// <summary>
        /// Gets or sets the Speed.
        /// </summary>
        /// <value>
        /// The Speed value.
        /// </value>  
        public int Speed
        {
            get => _Speed;
            set
            {
                _Speed = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Failed.
        /// </summary>
        /// <value>
        /// The Failed value.
        /// </value>  
        public int Failed
        {
            get => _Failed;
            set
            {
                _Failed = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the TotalSuccess.
        /// </summary>
        /// <value>
        /// The TotalSuccess value.
        /// </value>  
        public int TotalSuccess
        {
            get => _TotalSuccess;
            set
            {
                _TotalSuccess = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the P1SuccessTotal.
        /// </summary>
        /// <value>
        /// The P1SuccessTotal value.
        /// </value>  
        public int P1SuccessTotal
        {
            get => _P1SuccessTotal;
            set
            {
                _P1SuccessTotal = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the PupilSuccessTotal.
        /// </summary>
        /// <value>
        /// The PupilSuccessTotal value.
        /// </value>  
        public int PupilSuccessTotal
        {
            get => _PupilSuccessTotal;
            set
            {
                _PupilSuccessTotal = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Total.
        /// </summary>
        /// <value>
        /// The Total value.
        /// </value>  
        public int Total
        {
            get => _Total;
            set
            {
                _Total = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RunDate.
        /// </summary>
        /// <value>
        /// The RunDate value.
        /// </value>  
        public DateTime RunDate
        {
            get => _RunDate;
            set
            {
                _RunDate = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the TestRunKey.
        /// </summary>
        /// <value>
        /// The TestRunKey value.
        /// </value>  
        public Guid TestRunKey
        {
            get => _TestRunKey;
            set
            {
                _TestRunKey = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the success rate.
        /// </summary>
        /// <value>
        /// The success rate.
        /// </value>
        public int SuccessRate => (int)(TotalSuccess / (double)Total * 100d);

        /// <summary>
        /// Gets the fail rate.
        /// </summary>
        /// <value>
        /// The fail rate.
        /// </value>
        public int FailRate => (int)(Failed / (double)Total * 100d);
    }
}