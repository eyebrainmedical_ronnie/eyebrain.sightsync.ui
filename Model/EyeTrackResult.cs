﻿using System;
using System.Threading.Tasks;
using Eybrain.SightSynch.UI.Database;

namespace Eybrain.SightSynch.UI.Model
{
    /// <summary>
    /// Eye Tracking Result
    /// </summary>
    public class EyeTrackResult
    {
        public Guid EyeTrackResultKey { get; set; }
        public Guid PatientKey { get; set; }
        public DateTimeOffset TestDate { get; set; }
        public string Filename { get; set; }
        public double PupilarDistance { get; set; }
        public bool PupilLeftDetected { get; set; }
        public bool PupilRightDetected { get; set; }
        public bool P1LeftDetected { get; set; }
        public bool P1RightDetected { get; set; }
        public double? PupilCenterLeftX { get; set; }
        public double? PupilCenterLeftY { get; set; }
        public double? PupilCenterRightX { get; set; }
        public double? PupilCenterRightY { get; set; }
        public double? PurkinjeWidthLeft { get; set; }
        public double? PurkinjeWidthRight { get; set; }
        public double? PurkinjeLeftCenterX { get; set; }
        public double? PurkinjeLeftCenterY { get; set; }
        public double? PurkinjeRightCenterX { get; set; }
        public double? PurkinjeRightCenterY { get; set; }
        public double? RotationLeft { get; set; }
        public double? RotationRight { get; set; }


        /// <summary>
        /// Saves the asynchronous.
        /// </summary>
        public async Task SaveAsync()
        {
            await Data.Execute("EyeTracking.SaveResult",
                EyeTrackResultKey,
                PatientKey,
                TestDate,
                Filename,
                PupilarDistance,
                PupilLeftDetected,
                PupilRightDetected,
                P1LeftDetected,
                P1RightDetected,
                PupilCenterLeftX ?? 0,
                PupilCenterLeftY ?? 0,
                PupilCenterRightX ?? 0,
                PupilCenterRightY ?? 0,
                PurkinjeWidthLeft ?? 0,
                PurkinjeWidthRight ?? 0,
                PurkinjeLeftCenterX ?? 0,
                PurkinjeLeftCenterY ?? 0,
                PurkinjeRightCenterX ?? 0,
                PurkinjeRightCenterY ?? 0,
                RotationLeft ?? 0,
                RotationRight ?? 0
            );
        }
    }
}
