﻿using System;

namespace Eybrain.SightSynch.UI.Model
{
    public static class ResultStatuses
    {
        public static readonly Guid FailedP1 = Guid.Parse("FCA075C3-E3A1-4989-8748-22C69C2C1B82");
        public static readonly Guid InvalidFile = Guid.Parse("CE3CF6DA-0357-4C33-A1AA-7EF706628C1E");
        public static readonly Guid FailedPupil = Guid.Parse("789167D9-F2E2-43E5-9E0C-9363725261A5");
        public static readonly Guid Success = Guid.Parse("C557CAB6-9640-4DFD-B6FA-FBAEEB507735");
    }
}


/*

    select '    public static readonly Guid ' + replace(Description, ' ', '') + ' = Guid.Parse("' + convert(varchar(40), ResultStatusKey) + '");'
    from ResultStatus

 */
