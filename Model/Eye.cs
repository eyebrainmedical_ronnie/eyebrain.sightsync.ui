﻿namespace Eybrain.SightSynch.UI.Model
{
    /// <summary>
    /// The enumeration of types of eyes - if we encounter Aliens one day, then this code will need to be ameneded or if you believe in the 'third' eye, add it to the list
    /// </summary>
    public enum Eyes
    {
        Left = 0,
        Right = 1
    }
}
