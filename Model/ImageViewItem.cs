﻿using System.Drawing;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Model
{
    public class ImageViewItem
    {
        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        public EyeLocation Location { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        public object Host { get; set; }
    }
}