﻿using System.Collections.Generic;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Model
{
    /// <summary>
    /// Represents a test result for a specific patient
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.Model.TrackingBase{Eybrain.SightSynch.UI.Model.Patient}" />
    public class PatientEyeResult : TrackingBase<Patient>
    {
        /// <summary>
        /// Gets or sets the left eye results.
        /// </summary>
        /// <value>
        /// The left eye results.
        /// </value>
        public List<Eye> LeftEyeResults { get; } = new List<Eye>();

        /// <summary>
        /// Gets the righ eye results.
        /// </summary>
        /// <value>
        /// The righ eye results.
        /// </value>
        public List<Eye> RightEyeResults { get; } = new List<Eye>();

        /// <summary>
        /// Gets the left count.
        /// </summary>
        /// <value>
        /// The left count.
        /// </value>
        public int LeftCount => LeftEyeResults.Count;

        /// <summary>
        /// Gets the right count.
        /// </summary>
        /// <value>
        /// The right count.
        /// </value>
        public int RightCount => RightEyeResults.Count;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientEyeResult"/> class.
        /// </summary>
        /// <param name="patient">The patient.</param>
        public PatientEyeResult(Patient patient) 
            : base(patient)
        {
            
        }

        /// <summary>
        /// Resets this instance and clears out all results
        /// </summary>
        public void Reset()
        {
            LeftEyeResults.Clear();
            RightEyeResults.Clear();
        }
    }
}
