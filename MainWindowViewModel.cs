﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Eybrain.SightSynch.UI.Commands;
using Eybrain.SightSynch.UI.Interfaces;
using Eybrain.SightSynch.UI.Model;
using Eybrain.SightSynch.UI.Navgraph;
using Eybrain.SightSynch.UI.ViewModels;
using Eyebrain.SightSync.Framework;

namespace Eybrain.SightSynch.UI
{
    /// <summary>
    /// The viewmodel that holds all the functionality of the system
    /// </summary>
    /// <seealso cref="Eybrain.SightSynch.UI.ViewModels.BaseViewModel" />
    public class MainWindowViewModel : BaseViewModel, INavSource
    {
        private UserControl _CurrentView;
        private ObservableCollection<CommandItem> _TopCommands;
        private User _OperatorUser;
        private RelayCommand<object> _BackCommand;
        private bool _ShowBack;
        private UserControl _LeftEye;
        private UserControl _RightEye;
        private bool _IsNavigating;


        /// <summary>
        /// Gets or sets the IsNavigating.
        /// </summary>
        /// <value>
        /// The IsNavigating value.
        /// </value>  
        public bool IsNavigating
        {
            get => _IsNavigating;
            set
            {
                _IsNavigating = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RightEye.
        /// </summary>
        /// <value>
        /// The RightEye value.
        /// </value>  
        public UserControl RightEye
        {
            get => _RightEye;
            set
            {
                _RightEye = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the LeftEye.
        /// </summary>
        /// <value>
        /// The LeftEye value.
        /// </value>  
        public UserControl LeftEye
        {
            get => _LeftEye;
            set
            {
                _LeftEye = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the ShowBack.
        /// </summary>
        /// <value>
        /// The ShowBack value.
        /// </value>  
        public bool ShowBack
        {
            get => _ShowBack;
            set
            {
                _ShowBack = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the BackCommand.
        /// </summary>
        /// <value>
        /// The BackCommand value.
        /// </value>  
        public RelayCommand<object> BackCommand
        {
            get => _BackCommand ?? (_BackCommand = new RelayCommand<object>(OnBackCommand));
            set
            {
                _BackCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the OperatorUser.
        /// </summary>
        /// <value>
        /// The OperatorUser value.
        /// </value>  
        public User OperatorUser
        {
            get => _OperatorUser;
            set
            {
                _OperatorUser = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the TopCommands.
        /// </summary>
        /// <value>
        /// The TopCommands value.
        /// </value>  
        public ObservableCollection<CommandItem> TopCommands
        {
            get => _TopCommands ?? (_TopCommands = new ObservableCollection<CommandItem>());
            set
            {
                _TopCommands = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the CurrentView.
        /// </summary>
        /// <value>
        /// The CurrentView value.
        /// </value>
        public UserControl CurrentView
        {
            get => _CurrentView;
            set
            {
                _CurrentView = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when [activated].
        /// </summary>
        public override async void OnActivated()
        {
            Nav.Initialize<StandardGraph>(this);
            Nav.Go("splash");

            CommandHandler.Add(Factory.Make<LoadAssemblies>());         // Load the assemblies required to keep the system running
            CommandHandler.Add(Factory.Make<LoadConfiguration>());      // Load all configuration settings from files
            CommandHandler.Add(Factory.Make<InitailizeDevices>());      // Initialize all devices
            CommandHandler.Add(Factory.Make<SystemReady>());            // Set the system ready state

            Event.Register(EventType.GraphCreateFailure, GraphCreateFailed);
            Event.Register(EventType.GraphDoesNotExist, GraphDoesNotExist);
            Event.Register(EventType.SystemInitializeComplete, SystemInitialized);
            Event.Register(EventType.LogedOut, LoggedOut);
            Event.Register(EventType.LogedIn, LoggedIn);

            await CommandHandler.Execute();
        }

        /// <summary>
        /// Loggeds the in.
        /// </summary>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        private void LoggedIn(object arg1, object arg2)
        {
            TopCommands.Clear();
            Nav.Commands.ForEach(TopCommands.Add);
            OperatorUser = Context.Active;

            Nav.ShowOnLeftEye("internalsplash", null);
            Nav.ShowOnRightEye("internalsplash", null);
        }

        /// <summary>
        /// Loggeds the out.
        /// </summary>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        private void LoggedOut(object arg1, object arg2)
        {
            TopCommands.Clear();
            Nav.Commands.Where(w => !w.MustBeLoggedIn).ToList().ForEach(TopCommands.Add);
            OperatorUser = null;
        }

        /// <summary>
        /// Systems the initialized.
        /// </summary>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        private static void SystemInitialized(object arg1, object arg2)
        {
            Nav.Next();
        }

        /// <summary>
        /// Graphes the does not exist.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        private static void GraphDoesNotExist(object name, object value)
        {
            //TODO: Error Handling
        }

        /// <summary>
        /// Graphes the create failed.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        private static void GraphCreateFailed(object name, object value)
        {
            //TODO: Error Handling
        }

        /// <summary>
        /// Called when the system is closing down, here we have to clear all resources from all underlying systems.
        /// </summary>
        public override void OnDeactivated()
        {
            CommandHandler.Dispose(); // Dispose of all the drivers
            Nav.Deinitialized();
            Event.Clear();
        }

        /// <summary>
        /// Called when the user clicks the back command.
        /// </summary>
        /// <param name="o">The o.</param>
        private static void OnBackCommand(object o)
        {
            Nav.Back();
        }
    }
}