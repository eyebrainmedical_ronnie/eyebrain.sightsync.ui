﻿using System;

namespace Eybrain.SightSynch.UI
{
    public static class ExceptionExtentions
    {
        /// <summary>
        /// Gets the fullmessage.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static string GetFullmessage(this Exception ex)
        {
            var exception = ex;
            var res = "";

            while (exception != null)
            {
                res += $"{exception.Message}{Environment.NewLine}{exception.StackTrace}";
                exception = ex.InnerException;
            }

            return res;
        }
    }
}
