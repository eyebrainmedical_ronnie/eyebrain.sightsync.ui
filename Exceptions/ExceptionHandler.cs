﻿using System;
using System.Threading;
using System.Windows.Forms.ComponentModel.Com2Interop;
using Eybrain.SightSynch.UI.Interfaces;
using Eyebrain.SightSync.Framework;
using EyeBrain.SightSync.Common;

namespace Eybrain.SightSynch.UI.Exceptions
{
    /// <summary>
    /// Defines the exception boundary for the application
    /// </summary>
    [FactoryType(typeof(IError), true)]
    public class ErrorHandler : IError, IDisposable
    {
        private readonly IEvents _Events;

        /// <summary>
        /// Instantiate the exception boundary for the system
        /// </summary>
        public ErrorHandler()
        {
            _Events = Factory.Make<IEvents>();
            _Events.Register("Throw", Thread.CurrentThread, HandleException);
        }

        /// <summary>
        /// Handle the exception
        /// </summary>
        /// <param name="error"></param>
        private void HandleException(object error)
        {
            var exception = Instance.Off<Exception>(error);

            //TODO: Handle exception
        }

        /// <summary>
        /// Dispose this instance
        /// </summary>
        public void Dispose()
        {
            _Events.Deregister("Throw", HandleException);
        }
    }
}
